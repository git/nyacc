;; nyacc/lang/c99/c99-08.test           -*- scheme -*-
;;
;; Copyright (C) 2024 Matthew Wette
;;
;; Copying and distribution of this file, with or without modification,
;; are permitted in any medium without royalty provided the copyright
;; notice and this notice are preserved.  This file is offered as-is,
;; without any warranty.

;; test constant expression eval's

(define-module (c99-08)
  #:use-module ((system foreign) #:prefix ffi:)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 popen)
  #:use-module (test-suite lib)
  #:use-module (nyacc lang c99 pprint)
  #:use-module (nyacc lang sx-util)
  #:use-module (nyacc lang c99 parser)
  #:use-module (nyacc lang c99 cxeval)
  #:use-module (nyacc lang c99 munge))
(use-modules (ice-9 pretty-print))
(define pp pretty-print)
(define (ff fmt . args) (apply simple-format #f fmt args))

(define (fold p s l)
  (let loop ((s s) (l l))
    (if (null? l) s (loop (p (car l) s) (cdr l)))))

(define* (parse-string str)
  (with-input-from-string str parse-c99))

(define ctr
  (let ((p (make-count-reporter))) (register-reporter (car p)) ((cadr p))))
(register-reporter full-reporter)

(define (rand-bitfields n)
  (define types (list "short" "int" "long"))
  (define sizes (map (lambda (st ft) (cons st (* 8 (ffi:sizeof ft))))
                     types (list ffi:short ffi:int ffi:long)))
  (define (rbfdecl st ix)
    (let* ((bs (assoc-ref sizes st))
           (nb (random (1+ bs)))
           (vn (ff "v~S" ix)))
      (cond
       ((zero? (random 3)) (ff "  ~A ~A;\n" st vn))
       ((= nb bs) (ff "  ~A ~A;\n" st vn))
       ((zero? nb) (ff "  ~A :0;\n" st))
       (else (ff "  ~A ~A: ~A;\n" st vn nb)))))
  (let loop ((lines '()) (nn 1))
    (if (> nn n)
        (string-join (reverse lines) "")
        (loop (cons (rbfdecl (list-ref types (random 3)) nn) lines) (1+ nn)))))

(define (check-case n)
  (let* ((code (string-append "struct {\n" (rand-bitfields n) "} tt;\n"
                "long x = sizeof(tt);\n"))
         (prog (string-append "#include <stdio.h>\n" code
                              "int main() { printf(\"%ld\", x); }\n"))
         (tree (parse-string code))
         (udict (c99-trans-unit->udict tree))
         (expr (sx-ref* tree 2 2 1 2 1))
         (gcc-result
          (begin
            (with-output-to-file "ttx.c" (lambda () (display prog)))
            (system "gcc ttx.c")
            (string->number (get-string-all (open-input-pipe "./a.out")))))
         (c99-result (eval-c99-cx expr udict)))
    (equal? gcc-result c99-result)))

;; pretty-printer, parser test
;; We generate a tree, send it to the pretty-printer, then through the parser.
;; This should be a unit function, methinks.
(with-test-prefix "nyacc/c99-08, constant expression evaluation"

  (when (string=? %host-type "x86_64-pc-linux-gnu")
    (set! *random-state* (random-state-from-platform))

    ;; loop simple through pretty-printer
    (pass-if "sizeof"
      (let* ((code
              (string-append
               "struct foo { int iv; double dv; } x;\n"
               "long vsz = sizeof(x);"
               "long tsz = sizeof(struct foo);"))
             (tree (parse-string code))
             (udict (c99-trans-unit->udict tree))
             (val-vsz (eval-c99-cx (sx-ref* tree 2 2 1 2 1) udict))
             (val-tsz (eval-c99-cx (sx-ref* tree 3 2 1 2 1) udict)))
        (and (eq? val-vsz 16) (eq? val-tsz 16))))

    ;; bitfields
    #|
    (pass-if "sizeof w/ bitfields"
      (let loop ((n 25))
        (cond
         ((zero? n) #t)
         ((check-sizeof 9) (loop (1- n)))
         (else #f))))

    (pass-if "offsetof"
      (let* ((code
              (string-append
               "struct foo { int iv; double dv; } x;\n"
               "long tsz = __builtin_offsetof(struct foo, dv);"
               ))
             (tree (parse-string code))
             (udict (c99-trans-unit->udict tree))
             (exp-tos (sx-ref* tree 2 2 1 2 1))
             (val-tos (eval-c99-cx exp-tos udict))
             )
        (and (eq? osof-tsz 16) (eq? szof-tsv 16))
        ;;(pp exp-tos)
        ;;(pp val-tos)
        (equal? exp-tos val-tos)))
    |#

    ))

(exit (if (positive? (assq-ref ctr 'fail)) 1 0))
;; --- last line ---
