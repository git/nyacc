;; gobject.ffi				-*- Scheme -*-

;; Copyright (C) 2018,2024 Matthew Wette
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public License
;; along with this library; if not, see <http://www.gnu.org/licenses/>

(define-ffi-module (ffi-bs gobject)
  #:pkg-config "gobject-2.0"
  #:include '("glib-object.h")
  #:inc-filter (lambda (f p) (string-contains p "gobject/" 0))
  #:use-ffi-module (ffi-bs glib))

(define-public (g_signal_connect instance detailed_signal c_handler data)
  (g_signal_connect_data instance detailed_signal c_handler data NULL 0))

(define-public (g_signal_connect_after instance detailed_signal c_handler data)
  (g_signal_connect_data instance detailed_signal c_handler data NULL
			 'G_CONNECT_AFTER))

(define-public (g_signal_connect_swapped instance detailed_signal c_handler
					 data)
  (g_signal_connect_data instance detailed_signal c_handler data NULL
			 'G_CONNECT_SWAPPED))

(define* (g-register-static-simple
          p-type t-name c-size c-init i-size i-init #:optional (flags 0))
  (let ((p-type (if (zero? p-type) (g_object_get_type) p-type))
        (t-name (g_intern_string t-name))
        (c-init (ffi:procedure->pointer ffi:void c-init (list '*)))
        (i-init (ffi:procedure->pointer ffi:void i-init (list '*))))
    ;; signature does not use function types, but rather void*
    (g_type_register_static_simple
     p-type t-name c-size c-init i-size i-init flags)))
(export g-register-static-simple)

;; --- last line ---
