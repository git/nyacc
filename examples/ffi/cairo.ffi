;; cairo.ffi				-*- Scheme -*-

;; Copyright (C) 2018,2024 Matthew Wette
;;
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;;
;; This library is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; Lesser General Public License for more details.
;;
;; You should have received a copy of the GNU Lesser General Public License
;; along with this library; if not, see <http://www.gnu.org/licenses/>

(define-ffi-module (ffi cairo)
  #:pkg-config "cairo"
  #:include '("cairo.h"
	      ;;"cairo-ft.h"
	      ;;"cairo-gobject.h"
	      ;;"cairo-pdf.h"
	      ;;"cairo-ps.h"
	      ;;"cairo-quartz.h"
	      ;;"cairo-quartz-image.h"
	      ;;"cairo-script.h"
	      ;;"cairo-script-interpreter.h"
	      "cairo-svg.h"
	      ;;"cairo-tee.h"
	      ;;"cairo-xcb.h"
	      ;;"cairo-xlib.h"
	      ;;"cairo-xml.h"
	      )
  #:export (M_PI M_2PI make-cairo-unit-matrix))

(define-public cairo_raster_source_acquire_func_t*
  (cpointer  cairo_raster_source_acquire_func_t))

(define-public cairo_raster_source_release_func_t*
  (cpointer  cairo_raster_source_release_func_t))

#|
(define (make-cairo-unit-matrix)
  (make-cairo_matrix_t #(1.0 0.0 0.0 1.0 0.0 0.0)))
|#

(define M_PI 3.14159265358979323846)
(define M_2PI 6.28318530717958647692)

;; --- last line ---
