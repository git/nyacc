;; tsh-stmt-tab.scm

;; Copyright (C) 2021-2023 Matthew R. Wette
;; 
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;; See the file COPYING included with the this distribution.

(define tsh-stmt-mtab
  '(($start . 118) ("\n" . 3) (";" . 4) ($keyword . 5) ($keychar . 6) ($float 
    . 7) ($fixed . 8) ("::" . 9) (no-ws . 10) ($string . 11) ("," . 12) 
    ("]" . 13) ("[" . 14) ($deref . 15) ("~" . 16) ("!" . 17) ("%" . 18) 
    ("/" . 19) ("*" . 20) ("-" . 21) ("+" . 22) (">>" . 23) ("<<" . 24) 
    (">=" . 25) (">" . 26) ("<=" . 27) ("<" . 28) ("!=" . 29) ("==" . 30) 
    ("&" . 31) ("^" . 32) ("|" . 33) ("&&" . 34) ("||" . 35) ("default" . 36) 
    ("elseif" . 37) ("else" . 38) ("if" . 39) ("incr" . 40) ("return" . 41) 
    ("format" . 42) ("for" . 43) ("while" . 44) ("switch" . 45) ("lambda" . 46
    ) (")" . 47) ("(" . 48) ($deref/ix . 49) ("set" . 50) ($ident . 51) 
    ("args" . 52) ("local" . 53) ("nonlocal" . 54) ("global" . 55) ("}" . 56) 
    ("{" . 57) ("proc" . 58) ($lone-comm . 59) ("use" . 60) ("source" . 61) 
    ($error . 2) ($end . 63) ($code-comm . 64)))

(define tsh-stmt-ntab
  '((65 . path-1) (66 . expr-seq-1) (67 . expression) (68 . expr-list-1) 
    (69 . keyword) (70 . keychar) (71 . symbol) (72 . float) (73 . fixed) 
    (74 . unary-expression) (75 . multiplicative-expression) (76 . 
    additive-expression) (77 . shift-expression) (78 . relational-expression) 
    (79 . equality-expression) (80 . bitwise-and-expression) (81 . 
    bitwise-xor-expression) (82 . bitwise-or-expression) (83 . 
    logical-and-expression) (84 . logical-or-expression) (85 . 
    primary-expression) (86 . case-expr) (87 . default-case-expr) (88 . 
    case-list-1) (89 . elseif-list-1) (90 . elseif-list) (91 . block-stmt-list
    ) (92 . case-list) (93 . if-stmt) (94 . expr-seq) (95 . expr-list) 
    (96 . name-seq-1) (97 . unit-expr) (98 . arg-list-1) (99 . name-seq) 
    (100 . proc-stmt-list) (101 . arg-list) (102 . ident) (103 . 
    exec-stmt-list-1) (104 . decl-stmt-list/term-1) (105 . lone-comm) (106 . 
    fill-stmt-list/term-1) (107 . exec-stmt-list) (108 . decl-stmt-list/term) 
    (109 . fill-stmt-list/term) (110 . fill-stmt) (111 . exec-stmt) (112 . 
    decl-stmt) (113 . path) (114 . string) (115 . stmt) (116 . term) (117 . 
    topl-decl) (118 . item) (119 . script-1) (120 . script) (121 . top)))

(define tsh-stmt-len-v
  #(1 1 1 1 2 2 2 2 2 1 1 1 3 2 2 1 0 2 1 0 1 1 2 3 2 1 2 3 3 2 1 1 3 3 2 8 2
    2 2 1 0 2 5 2 1 1 2 3 6 2 7 3 1 5 5 13 2 1 2 2 3 5 9 6 10 1 5 6 1 2 1 1 2 
    2 2 4 2 1 1 1 3 1 3 1 3 1 3 1 3 1 3 3 1 3 3 3 3 1 3 3 1 3 3 1 3 3 3 1 2 2 
    2 2 1 4 1 1 1 1 1 1 3 3 1 1 3 1 0 2 1 1 1 5 5 1 1 1 1 1 1 1 1 1 1))

(define tsh-stmt-rto-v
  #(#f 121 120 119 119 118 118 117 117 115 115 115 100 100 100 100 100 91 91 
    110 110 109 106 106 106 108 104 104 104 104 107 103 103 103 103 112 112 
    112 112 101 98 98 98 98 99 96 96 111 111 111 111 111 111 111 111 111 111 
    111 111 111 111 93 93 93 93 90 89 89 92 92 88 88 88 88 86 86 87 97 67 84 
    84 83 83 82 82 81 81 80 80 79 79 79 78 78 78 78 78 77 77 77 76 76 76 75 75
    75 75 74 74 74 74 74 85 85 85 85 85 85 85 85 85 85 95 68 68 94 66 66 113 
    65 65 65 65 102 73 72 114 71 70 69 105 116 116))

(define tsh-stmt-pat-v
  #(((51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) (43 . 6) (44 . 7) (45 . 8) 
    (93 . 9) (48 . 10) (46 . 11) (102 . 12) (50 . 13) (53 . 14) (54 . 15) 
    (55 . 16) (58 . 17) (59 . 18) (110 . 19) (111 . 20) (112 . 21) (60 . 22) 
    (61 . 23) (115 . 24) (117 . 25) (118 . 26) (1 . -19)) ((1 . -133)) 
    ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) (7 . 51) (8 . 52) 
    (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) (114 . 58) (72 . 59) 
    (73 . 60) (49 . 61) (15 . 62) (85 . 82) (97 . 89)) ((51 . 1) (102 . 88)) 
    ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) (7 . 51) (8 . 52) 
    (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) (114 . 58) (72 . 59) 
    (73 . 60) (49 . 61) (15 . 62) (85 . 82) (97 . 87) (1 . -57)) ((66 . 45) 
    (94 . 86) (1 . -126)) ((57 . 85)) ((51 . 1) (5 . 48) (6 . 49) (102 . 50) 
    (11 . 31) (7 . 51) (8 . 52) (14 . 53) (48 . 54) (69 . 55) (70 . 56) 
    (71 . 57) (114 . 58) (72 . 59) (73 . 60) (49 . 61) (15 . 62) (85 . 82) 
    (97 . 84)) ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) (7 . 51) 
    (8 . 52) (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) (114 . 58) 
    (72 . 59) (73 . 60) (49 . 61) (15 . 62) (85 . 82) (97 . 83)) ((1 . -52)) 
    ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) (7 . 51) (8 . 52) 
    (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) (114 . 58) (72 . 59) 
    (73 . 60) (49 . 61) (15 . 62) (16 . 63) (17 . 64) (22 . 65) (21 . 66) 
    (85 . 67) (74 . 68) (75 . 69) (76 . 70) (77 . 71) (78 . 72) (79 . 73) 
    (80 . 74) (81 . 75) (82 . 76) (83 . 77) (84 . 78) (67 . 79) (68 . 80) 
    (95 . 81)) ((57 . 47)) ((66 . 45) (94 . 46) (1 . -126)) ((51 . 1) (102 . 
    43) (49 . 44)) ((51 . 38) (96 . 39) (99 . 42)) ((51 . 38) (96 . 39) 
    (99 . 41)) ((51 . 38) (96 . 39) (99 . 40)) ((51 . 1) (102 . 37)) ((1 . -20
    )) ((1 . -11)) ((1 . -10)) ((1 . -9)) ((11 . 33) (51 . 34) (65 . 35) 
    (113 . 36)) ((11 . 31) (114 . 32)) ((3 . 27) (4 . 28) (116 . 30)) (
    (3 . 27) (4 . 28) (116 . 29)) ((63 . 0)) ((1 . -142)) ((1 . -141)) 
    ((1 . -5)) ((1 . -6)) ((1 . -136)) ((1 . -7)) ((10 . -130) (1 . -130)) 
    ((10 . -129) (1 . -129)) ((10 . 135) (1 . -128)) ((1 . -8)) ((57 . 134)) 
    ((1 . -45)) ((51 . 133) (1 . -44)) ((1 . -36)) ((1 . -37)) ((1 . -38)) 
    ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) (7 . 51) (8 . 52) 
    (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) (114 . 58) (72 . 59) 
    (73 . 60) (49 . 61) (15 . 62) (85 . 82) (97 . 132)) ((48 . 131)) ((51 . 1)
    (5 . 48) (6 . 49) (102 . 50) (11 . 31) (7 . 51) (8 . 52) (14 . 53) 
    (48 . 54) (69 . 55) (70 . 56) (71 . 57) (114 . 58) (72 . 59) (73 . 60) 
    (49 . 61) (15 . 62) (85 . 130) (1 . -125)) ((1 . -49)) ((98 . 128) 
    (101 . 129) (1 . -40)) ((1 . -139)) ((1 . -138)) ((1 . -137)) ((1 . -135))
    ((1 . -134)) ((51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) (43 . 6) 
    (44 . 7) (45 . 8) (93 . 9) (48 . 10) (46 . 11) (102 . 12) (50 . 13) 
    (111 . 127)) ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) (7 . 51) 
    (8 . 52) (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) (114 . 58) 
    (72 . 59) (73 . 60) (49 . 61) (15 . 62) (16 . 63) (17 . 64) (22 . 65) 
    (21 . 66) (85 . 67) (74 . 68) (75 . 69) (76 . 70) (77 . 71) (78 . 72) 
    (79 . 73) (80 . 74) (81 . 75) (82 . 76) (83 . 77) (84 . 78) (67 . 79) 
    (68 . 80) (95 . 126)) ((1 . -119)) ((1 . -118)) ((1 . -117)) ((1 . -116)) 
    ((1 . -115)) ((1 . -114)) ((48 . 125)) ((1 . -112)) ((51 . 1) (5 . 48) 
    (6 . 49) (102 . 50) (11 . 31) (7 . 51) (8 . 52) (14 . 53) (48 . 54) 
    (69 . 55) (70 . 56) (71 . 57) (114 . 58) (72 . 59) (73 . 60) (49 . 61) 
    (15 . 62) (16 . 63) (17 . 64) (22 . 65) (21 . 66) (85 . 67) (74 . 124)) 
    ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) (7 . 51) (8 . 52) 
    (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) (114 . 58) (72 . 59) 
    (73 . 60) (49 . 61) (15 . 62) (16 . 63) (17 . 64) (22 . 65) (21 . 66) 
    (85 . 67) (74 . 123)) ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) 
    (7 . 51) (8 . 52) (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) 
    (114 . 58) (72 . 59) (73 . 60) (49 . 61) (15 . 62) (16 . 63) (17 . 64) 
    (22 . 65) (21 . 66) (85 . 67) (74 . 122)) ((51 . 1) (5 . 48) (6 . 49) 
    (102 . 50) (11 . 31) (7 . 51) (8 . 52) (14 . 53) (48 . 54) (69 . 55) 
    (70 . 56) (71 . 57) (114 . 58) (72 . 59) (73 . 60) (49 . 61) (15 . 62) 
    (16 . 63) (17 . 64) (22 . 65) (21 . 66) (85 . 67) (74 . 121)) ((1 . -107))
    ((1 . -103)) ((20 . 118) (19 . 119) (18 . 120) (1 . -100)) ((22 . 116) 
    (21 . 117) (1 . -97)) ((24 . 114) (23 . 115) (1 . -92)) ((28 . 110) 
    (27 . 111) (26 . 112) (25 . 113) (1 . -89)) ((30 . 108) (29 . 109) 
    (1 . -87)) ((31 . 107) (1 . -85)) ((32 . 106) (1 . -83)) ((33 . 105) 
    (1 . -81)) ((34 . 104) (1 . -79)) ((35 . 103) (1 . -78)) ((1 . -123)) 
    ((12 . 102) (1 . -122)) ((47 . 101)) ((1 . -77)) ((57 . 100)) ((57 . 99)) 
    ((51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) (43 . 6) (44 . 7) (45 . 8) 
    (93 . 9) (48 . 10) (46 . 11) (102 . 12) (50 . 13) (59 . 18) (111 . 92) 
    (110 . 93) (103 . 94) (106 . 95) (107 . 96) (109 . 97) (91 . 98) (1 . -19)
    ) ((1 . -56)) ((1 . -58)) ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31)
    (7 . 51) (8 . 52) (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) 
    (114 . 58) (72 . 59) (73 . 60) (49 . 61) (15 . 62) (85 . 82) (97 . 91) 
    (1 . -59)) ((57 . 90)) ((51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) 
    (43 . 6) (44 . 7) (45 . 8) (93 . 9) (48 . 10) (46 . 11) (102 . 12) 
    (50 . 13) (59 . 18) (111 . 92) (110 . 93) (103 . 94) (106 . 95) (107 . 96)
    (109 . 97) (91 . 178) (1 . -19)) ((1 . -60)) ((1 . -31)) ((3 . 27) 
    (4 . 28) (116 . 177)) ((3 . 27) (4 . 28) (116 . 176) (1 . -30)) ((59 . 173
    ) (105 . 174) (3 . 27) (4 . 28) (116 . 175) (1 . -21)) ((1 . -18)) 
    ((51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) (43 . 6) (44 . 7) (45 . 8) 
    (93 . 9) (48 . 10) (46 . 11) (102 . 12) (50 . 13) (111 . 92) (103 . 94) 
    (107 . 172)) ((56 . 171)) ((51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) 
    (43 . 6) (44 . 7) (45 . 8) (93 . 9) (48 . 10) (46 . 11) (102 . 12) 
    (50 . 13) (59 . 18) (111 . 92) (110 . 93) (103 . 94) (106 . 95) (107 . 96)
    (109 . 97) (91 . 170) (1 . -19)) ((51 . 1) (5 . 48) (6 . 49) (102 . 50) 
    (11 . 31) (7 . 51) (8 . 52) (14 . 53) (48 . 54) (69 . 55) (70 . 56) 
    (71 . 57) (114 . 58) (72 . 59) (73 . 60) (49 . 61) (15 . 62) (85 . 82) 
    (3 . 27) (4 . 28) (97 . 165) (116 . 166) (86 . 167) (88 . 168) (92 . 169))
    ((1 . -51)) ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) (7 . 51) 
    (8 . 52) (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) (114 . 58) 
    (72 . 59) (73 . 60) (49 . 61) (15 . 62) (16 . 63) (17 . 64) (22 . 65) 
    (21 . 66) (85 . 67) (74 . 68) (75 . 69) (76 . 70) (77 . 71) (78 . 72) 
    (79 . 73) (80 . 74) (81 . 75) (82 . 76) (83 . 77) (84 . 78) (67 . 164)) 
    ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) (7 . 51) (8 . 52) 
    (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) (114 . 58) (72 . 59) 
    (73 . 60) (49 . 61) (15 . 62) (16 . 63) (17 . 64) (22 . 65) (21 . 66) 
    (85 . 67) (74 . 68) (75 . 69) (76 . 70) (77 . 71) (78 . 72) (79 . 73) 
    (80 . 74) (81 . 75) (82 . 76) (83 . 163)) ((51 . 1) (5 . 48) (6 . 49) 
    (102 . 50) (11 . 31) (7 . 51) (8 . 52) (14 . 53) (48 . 54) (69 . 55) 
    (70 . 56) (71 . 57) (114 . 58) (72 . 59) (73 . 60) (49 . 61) (15 . 62) 
    (16 . 63) (17 . 64) (22 . 65) (21 . 66) (85 . 67) (74 . 68) (75 . 69) 
    (76 . 70) (77 . 71) (78 . 72) (79 . 73) (80 . 74) (81 . 75) (82 . 162)) 
    ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) (7 . 51) (8 . 52) 
    (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) (114 . 58) (72 . 59) 
    (73 . 60) (49 . 61) (15 . 62) (16 . 63) (17 . 64) (22 . 65) (21 . 66) 
    (85 . 67) (74 . 68) (75 . 69) (76 . 70) (77 . 71) (78 . 72) (79 . 73) 
    (80 . 74) (81 . 161)) ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) 
    (7 . 51) (8 . 52) (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) 
    (114 . 58) (72 . 59) (73 . 60) (49 . 61) (15 . 62) (16 . 63) (17 . 64) 
    (22 . 65) (21 . 66) (85 . 67) (74 . 68) (75 . 69) (76 . 70) (77 . 71) 
    (78 . 72) (79 . 73) (80 . 160)) ((51 . 1) (5 . 48) (6 . 49) (102 . 50) 
    (11 . 31) (7 . 51) (8 . 52) (14 . 53) (48 . 54) (69 . 55) (70 . 56) 
    (71 . 57) (114 . 58) (72 . 59) (73 . 60) (49 . 61) (15 . 62) (16 . 63) 
    (17 . 64) (22 . 65) (21 . 66) (85 . 67) (74 . 68) (75 . 69) (76 . 70) 
    (77 . 71) (78 . 72) (79 . 159)) ((51 . 1) (5 . 48) (6 . 49) (102 . 50) 
    (11 . 31) (7 . 51) (8 . 52) (14 . 53) (48 . 54) (69 . 55) (70 . 56) 
    (71 . 57) (114 . 58) (72 . 59) (73 . 60) (49 . 61) (15 . 62) (16 . 63) 
    (17 . 64) (22 . 65) (21 . 66) (85 . 67) (74 . 68) (75 . 69) (76 . 70) 
    (77 . 71) (78 . 158)) ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) 
    (7 . 51) (8 . 52) (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) 
    (114 . 58) (72 . 59) (73 . 60) (49 . 61) (15 . 62) (16 . 63) (17 . 64) 
    (22 . 65) (21 . 66) (85 . 67) (74 . 68) (75 . 69) (76 . 70) (77 . 71) 
    (78 . 157)) ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) (7 . 51) 
    (8 . 52) (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) (114 . 58) 
    (72 . 59) (73 . 60) (49 . 61) (15 . 62) (16 . 63) (17 . 64) (22 . 65) 
    (21 . 66) (85 . 67) (74 . 68) (75 . 69) (76 . 70) (77 . 156)) ((51 . 1) 
    (5 . 48) (6 . 49) (102 . 50) (11 . 31) (7 . 51) (8 . 52) (14 . 53) 
    (48 . 54) (69 . 55) (70 . 56) (71 . 57) (114 . 58) (72 . 59) (73 . 60) 
    (49 . 61) (15 . 62) (16 . 63) (17 . 64) (22 . 65) (21 . 66) (85 . 67) 
    (74 . 68) (75 . 69) (76 . 70) (77 . 155)) ((51 . 1) (5 . 48) (6 . 49) 
    (102 . 50) (11 . 31) (7 . 51) (8 . 52) (14 . 53) (48 . 54) (69 . 55) 
    (70 . 56) (71 . 57) (114 . 58) (72 . 59) (73 . 60) (49 . 61) (15 . 62) 
    (16 . 63) (17 . 64) (22 . 65) (21 . 66) (85 . 67) (74 . 68) (75 . 69) 
    (76 . 70) (77 . 154)) ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) 
    (7 . 51) (8 . 52) (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) 
    (114 . 58) (72 . 59) (73 . 60) (49 . 61) (15 . 62) (16 . 63) (17 . 64) 
    (22 . 65) (21 . 66) (85 . 67) (74 . 68) (75 . 69) (76 . 70) (77 . 153)) 
    ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) (7 . 51) (8 . 52) 
    (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) (114 . 58) (72 . 59) 
    (73 . 60) (49 . 61) (15 . 62) (16 . 63) (17 . 64) (22 . 65) (21 . 66) 
    (85 . 67) (74 . 68) (75 . 69) (76 . 152)) ((51 . 1) (5 . 48) (6 . 49) 
    (102 . 50) (11 . 31) (7 . 51) (8 . 52) (14 . 53) (48 . 54) (69 . 55) 
    (70 . 56) (71 . 57) (114 . 58) (72 . 59) (73 . 60) (49 . 61) (15 . 62) 
    (16 . 63) (17 . 64) (22 . 65) (21 . 66) (85 . 67) (74 . 68) (75 . 69) 
    (76 . 151)) ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) (7 . 51) 
    (8 . 52) (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) (114 . 58) 
    (72 . 59) (73 . 60) (49 . 61) (15 . 62) (16 . 63) (17 . 64) (22 . 65) 
    (21 . 66) (85 . 67) (74 . 68) (75 . 150)) ((51 . 1) (5 . 48) (6 . 49) 
    (102 . 50) (11 . 31) (7 . 51) (8 . 52) (14 . 53) (48 . 54) (69 . 55) 
    (70 . 56) (71 . 57) (114 . 58) (72 . 59) (73 . 60) (49 . 61) (15 . 62) 
    (16 . 63) (17 . 64) (22 . 65) (21 . 66) (85 . 67) (74 . 68) (75 . 149)) 
    ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) (7 . 51) (8 . 52) 
    (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) (114 . 58) (72 . 59) 
    (73 . 60) (49 . 61) (15 . 62) (16 . 63) (17 . 64) (22 . 65) (21 . 66) 
    (85 . 67) (74 . 148)) ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) 
    (7 . 51) (8 . 52) (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) 
    (114 . 58) (72 . 59) (73 . 60) (49 . 61) (15 . 62) (16 . 63) (17 . 64) 
    (22 . 65) (21 . 66) (85 . 67) (74 . 147)) ((51 . 1) (5 . 48) (6 . 49) 
    (102 . 50) (11 . 31) (7 . 51) (8 . 52) (14 . 53) (48 . 54) (69 . 55) 
    (70 . 56) (71 . 57) (114 . 58) (72 . 59) (73 . 60) (49 . 61) (15 . 62) 
    (16 . 63) (17 . 64) (22 . 65) (21 . 66) (85 . 67) (74 . 146)) ((1 . -108))
    ((1 . -109)) ((1 . -110)) ((1 . -111)) ((51 . 1) (5 . 48) (6 . 49) 
    (102 . 50) (11 . 31) (7 . 51) (8 . 52) (14 . 53) (48 . 54) (69 . 55) 
    (70 . 56) (71 . 57) (114 . 58) (72 . 59) (73 . 60) (49 . 61) (15 . 62) 
    (16 . 63) (17 . 64) (22 . 65) (21 . 66) (85 . 67) (74 . 68) (75 . 69) 
    (76 . 70) (77 . 71) (78 . 72) (79 . 73) (80 . 74) (81 . 75) (82 . 76) 
    (83 . 77) (84 . 78) (67 . 79) (68 . 80) (95 . 145)) ((47 . 144)) ((13 . 
    143)) ((51 . 1) (102 . 140) (57 . 141) (52 . 142) (1 . -39)) ((56 . 139)) 
    ((1 . -127)) ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) (7 . 51) 
    (8 . 52) (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) (114 . 58) 
    (72 . 59) (73 . 60) (49 . 61) (15 . 62) (16 . 63) (17 . 64) (22 . 65) 
    (21 . 66) (85 . 67) (74 . 68) (75 . 69) (76 . 70) (77 . 71) (78 . 72) 
    (79 . 73) (80 . 74) (81 . 75) (82 . 76) (83 . 77) (84 . 78) (67 . 79) 
    (68 . 80) (95 . 138)) ((1 . -47)) ((1 . -46)) ((98 . 128) (101 . 137) 
    (1 . -40)) ((9 . 136)) ((10 . 197)) ((56 . 196)) ((47 . 195)) ((57 . 194))
    ((1 . -41)) ((51 . 1) (102 . 193)) ((1 . -43)) ((1 . -121)) ((1 . -120)) 
    ((47 . 192)) ((1 . -106)) ((1 . -105)) ((1 . -104)) ((20 . 118) (19 . 119)
    (18 . 120) (1 . -102)) ((20 . 118) (19 . 119) (18 . 120) (1 . -101)) 
    ((22 . 116) (21 . 117) (1 . -99)) ((22 . 116) (21 . 117) (1 . -98)) 
    ((24 . 114) (23 . 115) (1 . -96)) ((24 . 114) (23 . 115) (1 . -95)) 
    ((24 . 114) (23 . 115) (1 . -94)) ((24 . 114) (23 . 115) (1 . -93)) 
    ((28 . 110) (27 . 111) (26 . 112) (25 . 113) (1 . -91)) ((28 . 110) 
    (27 . 111) (26 . 112) (25 . 113) (1 . -90)) ((30 . 108) (29 . 109) 
    (1 . -88)) ((31 . 107) (1 . -86)) ((32 . 106) (1 . -84)) ((33 . 105) 
    (1 . -82)) ((34 . 104) (1 . -80)) ((1 . -124)) ((51 . 1) (5 . 48) (6 . 49)
    (102 . 50) (11 . 31) (7 . 51) (8 . 52) (14 . 53) (48 . 54) (69 . 55) 
    (70 . 56) (71 . 57) (114 . 58) (72 . 59) (73 . 60) (49 . 61) (15 . 62) 
    (85 . 82) (97 . 190) (57 . 191)) ((1 . -71)) ((1 . -70)) ((36 . 186) 
    (87 . 187) (51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) (7 . 51) 
    (8 . 52) (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) (114 . 58) 
    (72 . 59) (73 . 60) (49 . 61) (15 . 62) (85 . 82) (97 . 165) (86 . 188) 
    (3 . 27) (4 . 28) (116 . 189) (1 . -68)) ((56 . 185)) ((56 . 184)) 
    ((57 . 183)) ((1 . -17)) ((1 . -140)) ((3 . 27) (4 . 28) (116 . 182)) 
    ((1 . -24)) ((59 . 173) (105 . 180) (51 . 1) (39 . 2) (40 . 3) (41 . 4) 
    (42 . 5) (43 . 6) (44 . 7) (45 . 8) (93 . 9) (48 . 10) (46 . 11) (102 . 12
    ) (50 . 13) (111 . 181) (1 . -34)) ((1 . -22)) ((56 . 179)) ((38 . 212) 
    (37 . 213) (89 . 214) (90 . 215) (1 . -61)) ((1 . -33)) ((1 . -32)) 
    ((1 . -23)) ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) (7 . 51) 
    (8 . 52) (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) (114 . 58) 
    (72 . 59) (73 . 60) (49 . 61) (15 . 62) (85 . 82) (97 . 211)) ((1 . -54)) 
    ((1 . -53)) ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) (7 . 51) 
    (8 . 52) (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) (114 . 58) 
    (72 . 59) (73 . 60) (49 . 61) (15 . 62) (85 . 82) (97 . 210)) ((1 . -69)) 
    ((1 . -72)) ((1 . -73)) ((1 . -74)) ((51 . 1) (39 . 2) (40 . 3) (41 . 4) 
    (42 . 5) (43 . 6) (44 . 7) (45 . 8) (93 . 9) (48 . 10) (46 . 11) (102 . 12
    ) (50 . 13) (59 . 18) (111 . 92) (110 . 93) (103 . 94) (106 . 95) (107 . 
    96) (109 . 97) (91 . 209) (1 . -19)) ((1 . -113)) ((51 . 1) (5 . 48) 
    (6 . 49) (102 . 50) (11 . 31) (7 . 51) (8 . 52) (14 . 53) (48 . 54) 
    (69 . 55) (70 . 56) (71 . 57) (114 . 58) (72 . 59) (73 . 60) (49 . 61) 
    (15 . 62) (85 . 82) (97 . 208)) ((51 . 1) (39 . 2) (40 . 3) (41 . 4) 
    (42 . 5) (43 . 6) (44 . 7) (45 . 8) (93 . 9) (48 . 10) (46 . 11) (102 . 12
    ) (50 . 13) (53 . 14) (54 . 15) (55 . 16) (58 . 17) (59 . 18) (111 . 92) 
    (112 . 202) (110 . 93) (103 . 94) (104 . 203) (106 . 95) (107 . 204) 
    (108 . 205) (109 . 206) (100 . 207) (56 . -16) (1 . -19)) ((51 . 1) 
    (5 . 48) (6 . 49) (102 . 50) (11 . 31) (7 . 51) (8 . 52) (14 . 53) 
    (48 . 54) (69 . 55) (70 . 56) (71 . 57) (114 . 58) (72 . 59) (73 . 60) 
    (49 . 61) (15 . 62) (85 . 82) (97 . 201)) ((57 . 200)) ((11 . 198) 
    (51 . 199)) ((10 . -132) (1 . -132)) ((10 . -131) (1 . -131)) ((51 . 1) 
    (39 . 2) (40 . 3) (41 . 4) (42 . 5) (43 . 6) (44 . 7) (45 . 8) (93 . 9) 
    (48 . 10) (46 . 11) (102 . 12) (50 . 13) (53 . 14) (54 . 15) (55 . 16) 
    (58 . 17) (59 . 18) (111 . 92) (112 . 202) (110 . 93) (103 . 94) (104 . 
    203) (106 . 95) (107 . 204) (108 . 205) (109 . 206) (100 . 231) (56 . -16)
    (1 . -19)) ((1 . -48)) ((3 . 27) (4 . 28) (116 . 230)) ((53 . 14) (54 . 15
    ) (55 . 16) (58 . 17) (112 . 227) (59 . 173) (105 . 228) (3 . 27) (4 . 28)
    (116 . 229) (1 . -25)) ((1 . -15)) ((51 . 1) (39 . 2) (40 . 3) (41 . 4) 
    (42 . 5) (43 . 6) (44 . 7) (45 . 8) (93 . 9) (48 . 10) (46 . 11) (102 . 12
    ) (50 . 13) (111 . 92) (103 . 94) (107 . 226)) ((53 . 14) (54 . 15) 
    (55 . 16) (58 . 17) (112 . 202) (104 . 203) (108 . 224) (51 . 1) (39 . 2) 
    (40 . 3) (41 . 4) (42 . 5) (43 . 6) (44 . 7) (45 . 8) (93 . 9) (48 . 10) 
    (46 . 11) (102 . 12) (50 . 13) (111 . 92) (103 . 94) (107 . 225)) (
    (56 . 223)) ((56 . 222)) ((56 . 221)) ((1 . -76)) ((56 . 220)) ((57 . 219)
    ) ((51 . 1) (5 . 48) (6 . 49) (102 . 50) (11 . 31) (7 . 51) (8 . 52) 
    (14 . 53) (48 . 54) (69 . 55) (70 . 56) (71 . 57) (114 . 58) (72 . 59) 
    (73 . 60) (49 . 61) (15 . 62) (85 . 82) (97 . 218)) ((37 . 217) (1 . -65))
    ((38 . 216) (1 . -63)) ((57 . 240)) ((51 . 1) (5 . 48) (6 . 49) (102 . 50)
    (11 . 31) (7 . 51) (8 . 52) (14 . 53) (48 . 54) (69 . 55) (70 . 56) 
    (71 . 57) (114 . 58) (72 . 59) (73 . 60) (49 . 61) (15 . 62) (85 . 82) 
    (97 . 239)) ((57 . 238)) ((51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) 
    (43 . 6) (44 . 7) (45 . 8) (93 . 9) (48 . 10) (46 . 11) (102 . 12) 
    (50 . 13) (59 . 18) (111 . 92) (110 . 93) (103 . 94) (106 . 95) (107 . 96)
    (109 . 97) (91 . 237) (1 . -19)) ((57 . 236)) ((1 . -75)) ((1 . -42)) 
    ((1 . -50)) ((51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) (43 . 6) 
    (44 . 7) (45 . 8) (93 . 9) (48 . 10) (46 . 11) (102 . 12) (50 . 13) 
    (111 . 92) (103 . 94) (107 . 235)) ((1 . -14)) ((1 . -13)) ((3 . 27) 
    (4 . 28) (116 . 234)) ((3 . 27) (4 . 28) (116 . 233)) ((1 . -29)) (
    (1 . -26)) ((56 . 232)) ((1 . -35)) ((1 . -28)) ((1 . -27)) ((1 . -12)) 
    ((51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) (43 . 6) (44 . 7) (45 . 8) 
    (93 . 9) (48 . 10) (46 . 11) (102 . 12) (50 . 13) (59 . 18) (111 . 92) 
    (110 . 93) (103 . 94) (106 . 95) (107 . 96) (109 . 97) (91 . 245) (1 . -19
    )) ((56 . 244)) ((51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) (43 . 6) 
    (44 . 7) (45 . 8) (93 . 9) (48 . 10) (46 . 11) (102 . 12) (50 . 13) 
    (59 . 18) (111 . 92) (110 . 93) (103 . 94) (106 . 95) (107 . 96) (109 . 97
    ) (91 . 243) (1 . -19)) ((57 . 242)) ((51 . 1) (39 . 2) (40 . 3) (41 . 4) 
    (42 . 5) (43 . 6) (44 . 7) (45 . 8) (93 . 9) (48 . 10) (46 . 11) (102 . 12
    ) (50 . 13) (59 . 18) (111 . 92) (110 . 93) (103 . 94) (106 . 95) (107 . 
    96) (109 . 97) (91 . 241) (1 . -19)) ((56 . 249)) ((51 . 1) (39 . 2) 
    (40 . 3) (41 . 4) (42 . 5) (43 . 6) (44 . 7) (45 . 8) (93 . 9) (48 . 10) 
    (46 . 11) (102 . 12) (50 . 13) (59 . 18) (111 . 92) (110 . 93) (103 . 94) 
    (106 . 95) (107 . 96) (109 . 97) (91 . 248) (1 . -19)) ((56 . 247)) 
    ((1 . -62)) ((56 . 246)) ((57 . 251)) ((1 . -66)) ((56 . 250)) ((1 . -64))
    ((1 . -67)) ((51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) (43 . 6) 
    (44 . 7) (45 . 8) (93 . 9) (48 . 10) (46 . 11) (102 . 12) (50 . 13) 
    (59 . 18) (111 . 92) (110 . 93) (103 . 94) (106 . 95) (107 . 96) (109 . 97
    ) (91 . 252) (1 . -19)) ((56 . 253)) ((1 . -55))))

(define tsh-stmt-tables
  (list
   (cons 'mtab tsh-stmt-mtab)
   (cons 'ntab tsh-stmt-ntab)
   (cons 'len-v tsh-stmt-len-v)
   (cons 'rto-v tsh-stmt-rto-v)
   (cons 'pat-v tsh-stmt-pat-v)
   ))

;;; end tables
