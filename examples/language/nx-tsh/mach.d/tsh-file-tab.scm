;; tsh-file-tab.scm

;; Copyright (C) 2021-2023 Matthew R. Wette
;; 
;; This library is free software; you can redistribute it and/or
;; modify it under the terms of the GNU Lesser General Public
;; License as published by the Free Software Foundation; either
;; version 3 of the License, or (at your option) any later version.
;; See the file COPYING included with the this distribution.

(define tsh-file-mtab
  '(($start . 121) ("\n" . 3) (";" . 4) ($keyword . 5) ($keychar . 6) ($float 
    . 7) ($fixed . 8) ("::" . 9) (no-ws . 10) ($string . 11) ("," . 12) 
    ("]" . 13) ("[" . 14) ($deref . 15) ("~" . 16) ("!" . 17) ("%" . 18) 
    ("/" . 19) ("*" . 20) ("-" . 21) ("+" . 22) (">>" . 23) ("<<" . 24) 
    (">=" . 25) (">" . 26) ("<=" . 27) ("<" . 28) ("!=" . 29) ("==" . 30) 
    ("&" . 31) ("^" . 32) ("|" . 33) ("&&" . 34) ("||" . 35) ("default" . 36) 
    ("elseif" . 37) ("else" . 38) ("if" . 39) ("incr" . 40) ("return" . 41) 
    ("format" . 42) ("for" . 43) ("while" . 44) ("switch" . 45) ("lambda" . 46
    ) (")" . 47) ("(" . 48) ($deref/ix . 49) ("set" . 50) ($ident . 51) 
    ("args" . 52) ("local" . 53) ("nonlocal" . 54) ("global" . 55) ("}" . 56) 
    ("{" . 57) ("proc" . 58) ($lone-comm . 59) ("use" . 60) ("source" . 61) 
    ($error . 2) ($end . 63) ($code-comm . 64)))

(define tsh-file-ntab
  '((65 . path-1) (66 . expr-seq-1) (67 . expression) (68 . expr-list-1) 
    (69 . keyword) (70 . keychar) (71 . symbol) (72 . float) (73 . fixed) 
    (74 . unary-expression) (75 . multiplicative-expression) (76 . 
    additive-expression) (77 . shift-expression) (78 . relational-expression) 
    (79 . equality-expression) (80 . bitwise-and-expression) (81 . 
    bitwise-xor-expression) (82 . bitwise-or-expression) (83 . 
    logical-and-expression) (84 . logical-or-expression) (85 . 
    primary-expression) (86 . case-expr) (87 . default-case-expr) (88 . 
    case-list-1) (89 . elseif-list-1) (90 . elseif-list) (91 . block-stmt-list
    ) (92 . case-list) (93 . if-stmt) (94 . expr-seq) (95 . expr-list) 
    (96 . name-seq-1) (97 . unit-expr) (98 . arg-list-1) (99 . name-seq) 
    (100 . proc-stmt-list) (101 . arg-list) (102 . ident) (103 . 
    exec-stmt-list-1) (104 . decl-stmt-list/term-1) (105 . lone-comm) (106 . 
    fill-stmt-list/term-1) (107 . exec-stmt-list) (108 . decl-stmt-list/term) 
    (109 . fill-stmt-list/term) (110 . fill-stmt) (111 . exec-stmt) (112 . 
    decl-stmt) (113 . path) (114 . string) (115 . stmt) (116 . term) (117 . 
    topl-decl) (118 . item) (119 . script-1) (120 . script) (121 . top)))

(define tsh-file-len-v
  #(1 1 1 1 2 2 2 2 2 1 1 1 3 2 2 1 0 2 1 0 1 1 2 3 2 1 2 3 3 2 1 1 3 3 2 8 2
    2 2 1 0 2 5 2 1 1 2 3 6 2 7 3 1 5 5 13 2 1 2 2 3 5 9 6 10 1 5 6 1 2 1 1 2 
    2 2 4 2 1 1 1 3 1 3 1 3 1 3 1 3 1 3 3 1 3 3 3 3 1 3 3 1 3 3 1 3 3 3 1 2 2 
    2 2 1 4 1 1 1 1 1 1 3 3 1 1 3 1 0 2 1 1 1 5 5 1 1 1 1 1 1 1 1 1 1))

(define tsh-file-rto-v
  #(#f 121 120 119 119 118 118 117 117 115 115 115 100 100 100 100 100 91 91 
    110 110 109 106 106 106 108 104 104 104 104 107 103 103 103 103 112 112 
    112 112 101 98 98 98 98 99 96 96 111 111 111 111 111 111 111 111 111 111 
    111 111 111 111 93 93 93 93 90 89 89 92 92 88 88 88 88 86 86 87 97 67 84 
    84 83 83 82 82 81 81 80 80 79 79 79 78 78 78 78 78 77 77 77 76 76 76 75 75
    75 75 74 74 74 74 74 85 85 85 85 85 85 85 85 85 85 95 68 68 94 66 66 113 
    65 65 65 65 102 73 72 114 71 70 69 105 116 116))

(define tsh-file-pat-v
  #(((51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) (43 . 6) (44 . 7) (45 . 8) 
    (93 . 9) (48 . 10) (46 . 11) (102 . 12) (50 . 13) (53 . 14) (54 . 15) 
    (55 . 16) (58 . 17) (59 . 18) (110 . 19) (111 . 20) (112 . 21) (60 . 22) 
    (61 . 23) (115 . 24) (117 . 25) (118 . 26) (119 . 27) (120 . 28) (121 . 29
    ) (3 . -19) (1 . -19)) ((3 . -133) (1 . -133)) ((51 . 1) (5 . 52) (6 . 53)
    (102 . 54) (11 . 35) (7 . 55) (8 . 56) (14 . 57) (48 . 58) (69 . 59) 
    (70 . 60) (71 . 61) (114 . 62) (72 . 63) (73 . 64) (49 . 65) (15 . 66) 
    (85 . 86) (97 . 93)) ((51 . 1) (102 . 92)) ((51 . 1) (5 . 52) (6 . 53) 
    (102 . 54) (11 . 35) (7 . 55) (8 . 56) (14 . 57) (48 . 58) (69 . 59) 
    (70 . 60) (71 . 61) (114 . 62) (72 . 63) (73 . 64) (49 . 65) (15 . 66) 
    (85 . 86) (97 . 91) (3 . -57) (1 . -57)) ((66 . 49) (94 . 90) (3 . -126) 
    (1 . -126)) ((57 . 89)) ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) 
    (7 . 55) (8 . 56) (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) 
    (114 . 62) (72 . 63) (73 . 64) (49 . 65) (15 . 66) (85 . 86) (97 . 88)) 
    ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) (7 . 55) (8 . 56) 
    (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) (114 . 62) (72 . 63) 
    (73 . 64) (49 . 65) (15 . 66) (85 . 86) (97 . 87)) ((3 . -52) (1 . -52)) 
    ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) (7 . 55) (8 . 56) 
    (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) (114 . 62) (72 . 63) 
    (73 . 64) (49 . 65) (15 . 66) (16 . 67) (17 . 68) (22 . 69) (21 . 70) 
    (85 . 71) (74 . 72) (75 . 73) (76 . 74) (77 . 75) (78 . 76) (79 . 77) 
    (80 . 78) (81 . 79) (82 . 80) (83 . 81) (84 . 82) (67 . 83) (68 . 84) 
    (95 . 85)) ((57 . 51)) ((66 . 49) (94 . 50) (3 . -126) (1 . -126)) 
    ((51 . 1) (102 . 47) (49 . 48)) ((51 . 42) (96 . 43) (99 . 46)) ((51 . 42)
    (96 . 43) (99 . 45)) ((51 . 42) (96 . 43) (99 . 44)) ((51 . 1) (102 . 41))
    ((3 . -20) (1 . -20)) ((3 . -11) (1 . -11)) ((3 . -10) (1 . -10)) (
    (3 . -9) (1 . -9)) ((11 . 37) (51 . 38) (65 . 39) (113 . 40)) ((11 . 35) 
    (114 . 36)) ((3 . 31) (4 . 32) (116 . 34)) ((3 . 31) (4 . 32) (116 . 33)) 
    ((59 . -3) (3 . -3) (1 . -3)) ((51 . 1) (39 . 2) (40 . 3) (41 . 4) 
    (42 . 5) (43 . 6) (44 . 7) (45 . 8) (93 . 9) (48 . 10) (46 . 11) (102 . 12
    ) (50 . 13) (53 . 14) (54 . 15) (55 . 16) (58 . 17) (59 . 18) (110 . 19) 
    (111 . 20) (112 . 21) (60 . 22) (61 . 23) (115 . 24) (117 . 25) (118 . 30)
    (3 . -19) (63 . -2) (1 . -19)) ((1 . -1)) ((63 . 0)) ((59 . -4) (3 . -4) 
    (1 . -4)) ((59 . -142) (3 . -142) (1 . -142)) ((59 . -141) (3 . -141) 
    (1 . -141)) ((3 . -5) (59 . -5) (1 . -5)) ((3 . -6) (59 . -6) (1 . -6)) 
    ((3 . -136) (1 . -136)) ((3 . -7) (1 . -7)) ((3 . -130) (1 . -130)) 
    ((3 . -129) (1 . -129)) ((10 . 139) (3 . -128) (1 . -128)) ((3 . -8) 
    (1 . -8)) ((57 . 138)) ((3 . -45) (1 . -45)) ((51 . 137) (3 . -44) 
    (1 . -44)) ((3 . -36) (1 . -36)) ((3 . -37) (1 . -37)) ((3 . -38) (1 . -38
    )) ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) (7 . 55) (8 . 56) 
    (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) (114 . 62) (72 . 63) 
    (73 . 64) (49 . 65) (15 . 66) (85 . 86) (97 . 136)) ((48 . 135)) ((51 . 1)
    (5 . 52) (6 . 53) (102 . 54) (11 . 35) (7 . 55) (8 . 56) (14 . 57) 
    (48 . 58) (69 . 59) (70 . 60) (71 . 61) (114 . 62) (72 . 63) (73 . 64) 
    (49 . 65) (15 . 66) (85 . 134) (3 . -125) (1 . -125)) ((3 . -49) (1 . -49)
    ) ((98 . 132) (101 . 133) (1 . -40)) ((3 . -139) (1 . -139)) ((3 . -138) 
    (1 . -138)) ((3 . -137) (1 . -137)) ((3 . -135) (1 . -135)) ((3 . -134) 
    (1 . -134)) ((51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) (43 . 6) 
    (44 . 7) (45 . 8) (93 . 9) (48 . 10) (46 . 11) (102 . 12) (50 . 13) 
    (111 . 131)) ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) (7 . 55) 
    (8 . 56) (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) (114 . 62) 
    (72 . 63) (73 . 64) (49 . 65) (15 . 66) (16 . 67) (17 . 68) (22 . 69) 
    (21 . 70) (85 . 71) (74 . 72) (75 . 73) (76 . 74) (77 . 75) (78 . 76) 
    (79 . 77) (80 . 78) (81 . 79) (82 . 80) (83 . 81) (84 . 82) (67 . 83) 
    (68 . 84) (95 . 130)) ((3 . -119) (1 . -119)) ((3 . -118) (1 . -118)) 
    ((3 . -117) (1 . -117)) ((3 . -116) (1 . -116)) ((3 . -115) (1 . -115)) 
    ((3 . -114) (1 . -114)) ((48 . 129)) ((3 . -112) (1 . -112)) ((51 . 1) 
    (5 . 52) (6 . 53) (102 . 54) (11 . 35) (7 . 55) (8 . 56) (14 . 57) 
    (48 . 58) (69 . 59) (70 . 60) (71 . 61) (114 . 62) (72 . 63) (73 . 64) 
    (49 . 65) (15 . 66) (16 . 67) (17 . 68) (22 . 69) (21 . 70) (85 . 71) 
    (74 . 128)) ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) (7 . 55) 
    (8 . 56) (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) (114 . 62) 
    (72 . 63) (73 . 64) (49 . 65) (15 . 66) (16 . 67) (17 . 68) (22 . 69) 
    (21 . 70) (85 . 71) (74 . 127)) ((51 . 1) (5 . 52) (6 . 53) (102 . 54) 
    (11 . 35) (7 . 55) (8 . 56) (14 . 57) (48 . 58) (69 . 59) (70 . 60) 
    (71 . 61) (114 . 62) (72 . 63) (73 . 64) (49 . 65) (15 . 66) (16 . 67) 
    (17 . 68) (22 . 69) (21 . 70) (85 . 71) (74 . 126)) ((51 . 1) (5 . 52) 
    (6 . 53) (102 . 54) (11 . 35) (7 . 55) (8 . 56) (14 . 57) (48 . 58) 
    (69 . 59) (70 . 60) (71 . 61) (114 . 62) (72 . 63) (73 . 64) (49 . 65) 
    (15 . 66) (16 . 67) (17 . 68) (22 . 69) (21 . 70) (85 . 71) (74 . 125)) 
    ((1 . -107)) ((1 . -103)) ((20 . 122) (19 . 123) (18 . 124) (1 . -100)) 
    ((22 . 120) (21 . 121) (1 . -97)) ((24 . 118) (23 . 119) (1 . -92)) 
    ((28 . 114) (27 . 115) (26 . 116) (25 . 117) (1 . -89)) ((30 . 112) 
    (29 . 113) (1 . -87)) ((31 . 111) (1 . -85)) ((32 . 110) (1 . -83)) 
    ((33 . 109) (1 . -81)) ((34 . 108) (1 . -79)) ((35 . 107) (1 . -78)) 
    ((1 . -123)) ((12 . 106) (1 . -122)) ((47 . 105)) ((3 . -77) (1 . -77)) 
    ((57 . 104)) ((57 . 103)) ((51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) 
    (43 . 6) (44 . 7) (45 . 8) (93 . 9) (48 . 10) (46 . 11) (102 . 12) 
    (50 . 13) (59 . 18) (111 . 96) (110 . 97) (103 . 98) (106 . 99) (107 . 100
    ) (109 . 101) (91 . 102) (3 . -19) (1 . -19)) ((3 . -56) (1 . -56)) 
    ((3 . -58) (1 . -58)) ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) 
    (7 . 55) (8 . 56) (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) 
    (114 . 62) (72 . 63) (73 . 64) (49 . 65) (15 . 66) (85 . 86) (97 . 95) 
    (3 . -59) (1 . -59)) ((57 . 94)) ((51 . 1) (39 . 2) (40 . 3) (41 . 4) 
    (42 . 5) (43 . 6) (44 . 7) (45 . 8) (93 . 9) (48 . 10) (46 . 11) (102 . 12
    ) (50 . 13) (59 . 18) (111 . 96) (110 . 97) (103 . 98) (106 . 99) (107 . 
    100) (109 . 101) (91 . 182) (3 . -19) (1 . -19)) ((3 . -60) (1 . -60)) 
    ((3 . -31) (1 . -31)) ((3 . 31) (4 . 32) (116 . 181)) ((3 . 31) (4 . 32) 
    (116 . 180) (1 . -30)) ((59 . 177) (105 . 178) (3 . 31) (4 . 32) (116 . 
    179) (1 . -21)) ((1 . -18)) ((51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) 
    (43 . 6) (44 . 7) (45 . 8) (93 . 9) (48 . 10) (46 . 11) (102 . 12) 
    (50 . 13) (111 . 96) (103 . 98) (107 . 176)) ((56 . 175)) ((51 . 1) 
    (39 . 2) (40 . 3) (41 . 4) (42 . 5) (43 . 6) (44 . 7) (45 . 8) (93 . 9) 
    (48 . 10) (46 . 11) (102 . 12) (50 . 13) (59 . 18) (111 . 96) (110 . 97) 
    (103 . 98) (106 . 99) (107 . 100) (109 . 101) (91 . 174) (3 . -19) 
    (1 . -19)) ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) (7 . 55) 
    (8 . 56) (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) (114 . 62) 
    (72 . 63) (73 . 64) (49 . 65) (15 . 66) (85 . 86) (3 . 31) (4 . 32) 
    (97 . 169) (116 . 170) (86 . 171) (88 . 172) (92 . 173)) ((3 . -51) 
    (1 . -51)) ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) (7 . 55) 
    (8 . 56) (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) (114 . 62) 
    (72 . 63) (73 . 64) (49 . 65) (15 . 66) (16 . 67) (17 . 68) (22 . 69) 
    (21 . 70) (85 . 71) (74 . 72) (75 . 73) (76 . 74) (77 . 75) (78 . 76) 
    (79 . 77) (80 . 78) (81 . 79) (82 . 80) (83 . 81) (84 . 82) (67 . 168)) 
    ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) (7 . 55) (8 . 56) 
    (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) (114 . 62) (72 . 63) 
    (73 . 64) (49 . 65) (15 . 66) (16 . 67) (17 . 68) (22 . 69) (21 . 70) 
    (85 . 71) (74 . 72) (75 . 73) (76 . 74) (77 . 75) (78 . 76) (79 . 77) 
    (80 . 78) (81 . 79) (82 . 80) (83 . 167)) ((51 . 1) (5 . 52) (6 . 53) 
    (102 . 54) (11 . 35) (7 . 55) (8 . 56) (14 . 57) (48 . 58) (69 . 59) 
    (70 . 60) (71 . 61) (114 . 62) (72 . 63) (73 . 64) (49 . 65) (15 . 66) 
    (16 . 67) (17 . 68) (22 . 69) (21 . 70) (85 . 71) (74 . 72) (75 . 73) 
    (76 . 74) (77 . 75) (78 . 76) (79 . 77) (80 . 78) (81 . 79) (82 . 166)) 
    ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) (7 . 55) (8 . 56) 
    (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) (114 . 62) (72 . 63) 
    (73 . 64) (49 . 65) (15 . 66) (16 . 67) (17 . 68) (22 . 69) (21 . 70) 
    (85 . 71) (74 . 72) (75 . 73) (76 . 74) (77 . 75) (78 . 76) (79 . 77) 
    (80 . 78) (81 . 165)) ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) 
    (7 . 55) (8 . 56) (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) 
    (114 . 62) (72 . 63) (73 . 64) (49 . 65) (15 . 66) (16 . 67) (17 . 68) 
    (22 . 69) (21 . 70) (85 . 71) (74 . 72) (75 . 73) (76 . 74) (77 . 75) 
    (78 . 76) (79 . 77) (80 . 164)) ((51 . 1) (5 . 52) (6 . 53) (102 . 54) 
    (11 . 35) (7 . 55) (8 . 56) (14 . 57) (48 . 58) (69 . 59) (70 . 60) 
    (71 . 61) (114 . 62) (72 . 63) (73 . 64) (49 . 65) (15 . 66) (16 . 67) 
    (17 . 68) (22 . 69) (21 . 70) (85 . 71) (74 . 72) (75 . 73) (76 . 74) 
    (77 . 75) (78 . 76) (79 . 163)) ((51 . 1) (5 . 52) (6 . 53) (102 . 54) 
    (11 . 35) (7 . 55) (8 . 56) (14 . 57) (48 . 58) (69 . 59) (70 . 60) 
    (71 . 61) (114 . 62) (72 . 63) (73 . 64) (49 . 65) (15 . 66) (16 . 67) 
    (17 . 68) (22 . 69) (21 . 70) (85 . 71) (74 . 72) (75 . 73) (76 . 74) 
    (77 . 75) (78 . 162)) ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) 
    (7 . 55) (8 . 56) (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) 
    (114 . 62) (72 . 63) (73 . 64) (49 . 65) (15 . 66) (16 . 67) (17 . 68) 
    (22 . 69) (21 . 70) (85 . 71) (74 . 72) (75 . 73) (76 . 74) (77 . 75) 
    (78 . 161)) ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) (7 . 55) 
    (8 . 56) (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) (114 . 62) 
    (72 . 63) (73 . 64) (49 . 65) (15 . 66) (16 . 67) (17 . 68) (22 . 69) 
    (21 . 70) (85 . 71) (74 . 72) (75 . 73) (76 . 74) (77 . 160)) ((51 . 1) 
    (5 . 52) (6 . 53) (102 . 54) (11 . 35) (7 . 55) (8 . 56) (14 . 57) 
    (48 . 58) (69 . 59) (70 . 60) (71 . 61) (114 . 62) (72 . 63) (73 . 64) 
    (49 . 65) (15 . 66) (16 . 67) (17 . 68) (22 . 69) (21 . 70) (85 . 71) 
    (74 . 72) (75 . 73) (76 . 74) (77 . 159)) ((51 . 1) (5 . 52) (6 . 53) 
    (102 . 54) (11 . 35) (7 . 55) (8 . 56) (14 . 57) (48 . 58) (69 . 59) 
    (70 . 60) (71 . 61) (114 . 62) (72 . 63) (73 . 64) (49 . 65) (15 . 66) 
    (16 . 67) (17 . 68) (22 . 69) (21 . 70) (85 . 71) (74 . 72) (75 . 73) 
    (76 . 74) (77 . 158)) ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) 
    (7 . 55) (8 . 56) (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) 
    (114 . 62) (72 . 63) (73 . 64) (49 . 65) (15 . 66) (16 . 67) (17 . 68) 
    (22 . 69) (21 . 70) (85 . 71) (74 . 72) (75 . 73) (76 . 74) (77 . 157)) 
    ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) (7 . 55) (8 . 56) 
    (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) (114 . 62) (72 . 63) 
    (73 . 64) (49 . 65) (15 . 66) (16 . 67) (17 . 68) (22 . 69) (21 . 70) 
    (85 . 71) (74 . 72) (75 . 73) (76 . 156)) ((51 . 1) (5 . 52) (6 . 53) 
    (102 . 54) (11 . 35) (7 . 55) (8 . 56) (14 . 57) (48 . 58) (69 . 59) 
    (70 . 60) (71 . 61) (114 . 62) (72 . 63) (73 . 64) (49 . 65) (15 . 66) 
    (16 . 67) (17 . 68) (22 . 69) (21 . 70) (85 . 71) (74 . 72) (75 . 73) 
    (76 . 155)) ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) (7 . 55) 
    (8 . 56) (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) (114 . 62) 
    (72 . 63) (73 . 64) (49 . 65) (15 . 66) (16 . 67) (17 . 68) (22 . 69) 
    (21 . 70) (85 . 71) (74 . 72) (75 . 154)) ((51 . 1) (5 . 52) (6 . 53) 
    (102 . 54) (11 . 35) (7 . 55) (8 . 56) (14 . 57) (48 . 58) (69 . 59) 
    (70 . 60) (71 . 61) (114 . 62) (72 . 63) (73 . 64) (49 . 65) (15 . 66) 
    (16 . 67) (17 . 68) (22 . 69) (21 . 70) (85 . 71) (74 . 72) (75 . 153)) 
    ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) (7 . 55) (8 . 56) 
    (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) (114 . 62) (72 . 63) 
    (73 . 64) (49 . 65) (15 . 66) (16 . 67) (17 . 68) (22 . 69) (21 . 70) 
    (85 . 71) (74 . 152)) ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) 
    (7 . 55) (8 . 56) (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) 
    (114 . 62) (72 . 63) (73 . 64) (49 . 65) (15 . 66) (16 . 67) (17 . 68) 
    (22 . 69) (21 . 70) (85 . 71) (74 . 151)) ((51 . 1) (5 . 52) (6 . 53) 
    (102 . 54) (11 . 35) (7 . 55) (8 . 56) (14 . 57) (48 . 58) (69 . 59) 
    (70 . 60) (71 . 61) (114 . 62) (72 . 63) (73 . 64) (49 . 65) (15 . 66) 
    (16 . 67) (17 . 68) (22 . 69) (21 . 70) (85 . 71) (74 . 150)) ((1 . -108))
    ((1 . -109)) ((1 . -110)) ((1 . -111)) ((51 . 1) (5 . 52) (6 . 53) 
    (102 . 54) (11 . 35) (7 . 55) (8 . 56) (14 . 57) (48 . 58) (69 . 59) 
    (70 . 60) (71 . 61) (114 . 62) (72 . 63) (73 . 64) (49 . 65) (15 . 66) 
    (16 . 67) (17 . 68) (22 . 69) (21 . 70) (85 . 71) (74 . 72) (75 . 73) 
    (76 . 74) (77 . 75) (78 . 76) (79 . 77) (80 . 78) (81 . 79) (82 . 80) 
    (83 . 81) (84 . 82) (67 . 83) (68 . 84) (95 . 149)) ((47 . 148)) ((13 . 
    147)) ((51 . 1) (102 . 144) (57 . 145) (52 . 146) (1 . -39)) ((56 . 143)) 
    ((3 . -127) (1 . -127)) ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) 
    (7 . 55) (8 . 56) (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) 
    (114 . 62) (72 . 63) (73 . 64) (49 . 65) (15 . 66) (16 . 67) (17 . 68) 
    (22 . 69) (21 . 70) (85 . 71) (74 . 72) (75 . 73) (76 . 74) (77 . 75) 
    (78 . 76) (79 . 77) (80 . 78) (81 . 79) (82 . 80) (83 . 81) (84 . 82) 
    (67 . 83) (68 . 84) (95 . 142)) ((3 . -47) (1 . -47)) ((3 . -46) (1 . -46)
    ) ((98 . 132) (101 . 141) (1 . -40)) ((9 . 140)) ((10 . 201)) ((56 . 200))
    ((47 . 199)) ((57 . 198)) ((1 . -41)) ((51 . 1) (102 . 197)) ((1 . -43)) 
    ((3 . -121) (1 . -121)) ((3 . -120) (1 . -120)) ((47 . 196)) ((1 . -106)) 
    ((1 . -105)) ((1 . -104)) ((20 . 122) (19 . 123) (18 . 124) (1 . -102)) 
    ((20 . 122) (19 . 123) (18 . 124) (1 . -101)) ((22 . 120) (21 . 121) 
    (1 . -99)) ((22 . 120) (21 . 121) (1 . -98)) ((24 . 118) (23 . 119) 
    (1 . -96)) ((24 . 118) (23 . 119) (1 . -95)) ((24 . 118) (23 . 119) 
    (1 . -94)) ((24 . 118) (23 . 119) (1 . -93)) ((28 . 114) (27 . 115) 
    (26 . 116) (25 . 117) (1 . -91)) ((28 . 114) (27 . 115) (26 . 116) 
    (25 . 117) (1 . -90)) ((30 . 112) (29 . 113) (1 . -88)) ((31 . 111) 
    (1 . -86)) ((32 . 110) (1 . -84)) ((33 . 109) (1 . -82)) ((34 . 108) 
    (1 . -80)) ((1 . -124)) ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) 
    (7 . 55) (8 . 56) (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) 
    (114 . 62) (72 . 63) (73 . 64) (49 . 65) (15 . 66) (85 . 86) (97 . 194) 
    (57 . 195)) ((3 . -71) (1 . -71)) ((3 . -70) (1 . -70)) ((36 . 190) 
    (87 . 191) (51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) (7 . 55) 
    (8 . 56) (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) (114 . 62) 
    (72 . 63) (73 . 64) (49 . 65) (15 . 66) (85 . 86) (97 . 169) (86 . 192) 
    (3 . 31) (4 . 32) (116 . 193) (1 . -68)) ((56 . 189)) ((56 . 188)) 
    ((57 . 187)) ((1 . -17)) ((3 . -140) (1 . -140)) ((3 . 31) (4 . 32) 
    (116 . 186)) ((3 . -24) (59 . -24) (1 . -24)) ((59 . 177) (105 . 184) 
    (51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) (43 . 6) (44 . 7) (45 . 8) 
    (93 . 9) (48 . 10) (46 . 11) (102 . 12) (50 . 13) (111 . 185) (3 . -34) 
    (1 . -34)) ((3 . -22) (59 . -22) (1 . -22)) ((56 . 183)) ((38 . 216) 
    (37 . 217) (89 . 218) (90 . 219) (3 . -61) (1 . -61)) ((3 . -33) (1 . -33)
    ) ((3 . -32) (1 . -32)) ((3 . -23) (59 . -23) (1 . -23)) ((51 . 1) 
    (5 . 52) (6 . 53) (102 . 54) (11 . 35) (7 . 55) (8 . 56) (14 . 57) 
    (48 . 58) (69 . 59) (70 . 60) (71 . 61) (114 . 62) (72 . 63) (73 . 64) 
    (49 . 65) (15 . 66) (85 . 86) (97 . 215)) ((3 . -54) (1 . -54)) ((3 . -53)
    (1 . -53)) ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) (7 . 55) 
    (8 . 56) (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) (114 . 62) 
    (72 . 63) (73 . 64) (49 . 65) (15 . 66) (85 . 86) (97 . 214)) ((1 . -69)) 
    ((3 . -72) (1 . -72)) ((3 . -73) (1 . -73)) ((3 . -74) (1 . -74)) (
    (51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) (43 . 6) (44 . 7) (45 . 8) 
    (93 . 9) (48 . 10) (46 . 11) (102 . 12) (50 . 13) (59 . 18) (111 . 96) 
    (110 . 97) (103 . 98) (106 . 99) (107 . 100) (109 . 101) (91 . 213) 
    (3 . -19) (1 . -19)) ((3 . -113) (1 . -113)) ((51 . 1) (5 . 52) (6 . 53) 
    (102 . 54) (11 . 35) (7 . 55) (8 . 56) (14 . 57) (48 . 58) (69 . 59) 
    (70 . 60) (71 . 61) (114 . 62) (72 . 63) (73 . 64) (49 . 65) (15 . 66) 
    (85 . 86) (97 . 212)) ((51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) 
    (43 . 6) (44 . 7) (45 . 8) (93 . 9) (48 . 10) (46 . 11) (102 . 12) 
    (50 . 13) (53 . 14) (54 . 15) (55 . 16) (58 . 17) (59 . 18) (111 . 96) 
    (112 . 206) (110 . 97) (103 . 98) (104 . 207) (106 . 99) (107 . 208) 
    (108 . 209) (109 . 210) (100 . 211) (3 . -19) (56 . -16) (1 . -19)) 
    ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) (7 . 55) (8 . 56) 
    (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) (114 . 62) (72 . 63) 
    (73 . 64) (49 . 65) (15 . 66) (85 . 86) (97 . 205)) ((57 . 204)) ((11 . 
    202) (51 . 203)) ((3 . -132) (1 . -132)) ((3 . -131) (1 . -131)) ((51 . 1)
    (39 . 2) (40 . 3) (41 . 4) (42 . 5) (43 . 6) (44 . 7) (45 . 8) (93 . 9) 
    (48 . 10) (46 . 11) (102 . 12) (50 . 13) (53 . 14) (54 . 15) (55 . 16) 
    (58 . 17) (59 . 18) (111 . 96) (112 . 206) (110 . 97) (103 . 98) (104 . 
    207) (106 . 99) (107 . 208) (108 . 209) (109 . 210) (100 . 235) (3 . -19) 
    (56 . -16) (1 . -19)) ((3 . -48) (1 . -48)) ((3 . 31) (4 . 32) (116 . 234)
    ) ((53 . 14) (54 . 15) (55 . 16) (58 . 17) (112 . 231) (59 . 177) (105 . 
    232) (3 . 31) (4 . 32) (116 . 233) (1 . -25)) ((1 . -15)) ((51 . 1) 
    (39 . 2) (40 . 3) (41 . 4) (42 . 5) (43 . 6) (44 . 7) (45 . 8) (93 . 9) 
    (48 . 10) (46 . 11) (102 . 12) (50 . 13) (111 . 96) (103 . 98) (107 . 230)
    ) ((53 . 14) (54 . 15) (55 . 16) (58 . 17) (112 . 206) (104 . 207) 
    (108 . 228) (51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) (43 . 6) (44 . 7)
    (45 . 8) (93 . 9) (48 . 10) (46 . 11) (102 . 12) (50 . 13) (111 . 96) 
    (103 . 98) (107 . 229)) ((56 . 227)) ((56 . 226)) ((56 . 225)) ((1 . -76))
    ((56 . 224)) ((57 . 223)) ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35)
    (7 . 55) (8 . 56) (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) 
    (114 . 62) (72 . 63) (73 . 64) (49 . 65) (15 . 66) (85 . 86) (97 . 222)) 
    ((37 . 221) (3 . -65) (1 . -65)) ((38 . 220) (3 . -63) (1 . -63)) (
    (57 . 244)) ((51 . 1) (5 . 52) (6 . 53) (102 . 54) (11 . 35) (7 . 55) 
    (8 . 56) (14 . 57) (48 . 58) (69 . 59) (70 . 60) (71 . 61) (114 . 62) 
    (72 . 63) (73 . 64) (49 . 65) (15 . 66) (85 . 86) (97 . 243)) ((57 . 242))
    ((51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) (43 . 6) (44 . 7) (45 . 8) 
    (93 . 9) (48 . 10) (46 . 11) (102 . 12) (50 . 13) (59 . 18) (111 . 96) 
    (110 . 97) (103 . 98) (106 . 99) (107 . 100) (109 . 101) (91 . 241) 
    (3 . -19) (1 . -19)) ((57 . 240)) ((3 . -75) (1 . -75)) ((1 . -42)) 
    ((3 . -50) (1 . -50)) ((51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) 
    (43 . 6) (44 . 7) (45 . 8) (93 . 9) (48 . 10) (46 . 11) (102 . 12) 
    (50 . 13) (111 . 96) (103 . 98) (107 . 239)) ((1 . -14)) ((1 . -13)) 
    ((3 . 31) (4 . 32) (116 . 238)) ((3 . 31) (4 . 32) (116 . 237)) ((3 . -29)
    (59 . -29) (1 . -29)) ((3 . -26) (59 . -26) (1 . -26)) ((56 . 236)) 
    ((3 . -35) (1 . -35)) ((3 . -28) (59 . -28) (1 . -28)) ((3 . -27) (59 . 
    -27) (1 . -27)) ((1 . -12)) ((51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) 
    (43 . 6) (44 . 7) (45 . 8) (93 . 9) (48 . 10) (46 . 11) (102 . 12) 
    (50 . 13) (59 . 18) (111 . 96) (110 . 97) (103 . 98) (106 . 99) (107 . 100
    ) (109 . 101) (91 . 249) (3 . -19) (1 . -19)) ((56 . 248)) ((51 . 1) 
    (39 . 2) (40 . 3) (41 . 4) (42 . 5) (43 . 6) (44 . 7) (45 . 8) (93 . 9) 
    (48 . 10) (46 . 11) (102 . 12) (50 . 13) (59 . 18) (111 . 96) (110 . 97) 
    (103 . 98) (106 . 99) (107 . 100) (109 . 101) (91 . 247) (3 . -19) 
    (1 . -19)) ((57 . 246)) ((51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) 
    (43 . 6) (44 . 7) (45 . 8) (93 . 9) (48 . 10) (46 . 11) (102 . 12) 
    (50 . 13) (59 . 18) (111 . 96) (110 . 97) (103 . 98) (106 . 99) (107 . 100
    ) (109 . 101) (91 . 245) (3 . -19) (1 . -19)) ((56 . 253)) ((51 . 1) 
    (39 . 2) (40 . 3) (41 . 4) (42 . 5) (43 . 6) (44 . 7) (45 . 8) (93 . 9) 
    (48 . 10) (46 . 11) (102 . 12) (50 . 13) (59 . 18) (111 . 96) (110 . 97) 
    (103 . 98) (106 . 99) (107 . 100) (109 . 101) (91 . 252) (3 . -19) 
    (1 . -19)) ((56 . 251)) ((3 . -62) (1 . -62)) ((56 . 250)) ((57 . 255)) 
    ((3 . -66) (1 . -66)) ((56 . 254)) ((3 . -64) (1 . -64)) ((3 . -67) 
    (1 . -67)) ((51 . 1) (39 . 2) (40 . 3) (41 . 4) (42 . 5) (43 . 6) (44 . 7)
    (45 . 8) (93 . 9) (48 . 10) (46 . 11) (102 . 12) (50 . 13) (59 . 18) 
    (111 . 96) (110 . 97) (103 . 98) (106 . 99) (107 . 100) (109 . 101) 
    (91 . 256) (3 . -19) (1 . -19)) ((56 . 257)) ((3 . -55) (1 . -55))))

(define tsh-file-tables
  (list
   (cons 'mtab tsh-file-mtab)
   (cons 'ntab tsh-file-ntab)
   (cons 'len-v tsh-file-len-v)
   (cons 'rto-v tsh-file-rto-v)
   (cons 'pat-v tsh-file-pat-v)
   ))

;;; end tables
