<!DOCTYPE html>
<html>
<!-- Created by GNU Texinfo 7.1, https://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- Copyright (C) 2024 - Matthew Wette

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3 or
any later version published by the Free Software Foundation; with no
Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.  A
copy of the license is included with the distribution as COPYING.DOC. -->
<title>The C-data Module for Guile</title>

<meta name="description" content="The C-data Module for Guile">
<meta name="keywords" content="The C-data Module for Guile">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<meta name="viewport" content="width=device-width,initial-scale=1">

<link href="#Top" rel="start" title="Top">
<link href="dir.html#Top" rel="up" title="(dir)">
<link href="#Introduction" rel="next" title="Introduction">
<link href="dir.html#Top" rel="prev" title="(dir)">
<style type="text/css">
<!--
a.copiable-link {visibility: hidden; text-decoration: none; line-height: 0em}
div.example {margin-left: 3.2em}
span:hover a.copiable-link {visibility: visible}
strong.def-name {font-family: monospace; font-weight: bold; font-size: larger}
-->
</style>


</head>

<body lang="en">





<div class="top-level-extent" id="Top">
<div class="nav-panel">
<p>
Next: <a href="#Introduction" accesskey="n" rel="next">Introduction</a>, Previous: <a href="dir.html#Top" accesskey="p" rel="prev">(dir)</a>, Up: <a href="dir.html#Top" accesskey="u" rel="up">(dir)</a> &nbsp; </p>
</div>
<h1 class="top" id="CData-Reference-Manual"><span>CData Reference Manual<a class="copiable-link" href="#CData-Reference-Manual"> &para;</a></span></h1>
<br><p> Matt Wette
<br> October 2024
<br> 
<br> This is a user guide for the NYACC CData module for Guile.
</p>

<hr>
<a class="node" id="Introduction"></a><div class="nav-panel">
</div>
<h3 class="heading" id="Introduction-1"><span>Introduction<a class="copiable-link" href="#Introduction-1"> &para;</a></span></h3>

<p>The <em class="emph">cdata</em> module and its partner <em class="emph">arch-info</em> provide a
way to work with data originating from C libraries.  We hope module
is reasonably easy to understand and use.  Size and alignment is
tracked for all types.  Types are classified into the following kinds:
base, struct, union, array, pointer, enum and function.  The procedures
<code class="code">cbase</code>, <code class="code">cstruct</code>, <code class="code">cunion</code>, <code class="code">cpointer</code>,
<code class="code">carray</code>, <code class="code">cenum</code> and <code class="code">cfunction</code> generate <em class="emph">ctype</em>
objects, and the procedure <code class="code">make-cdata</code> will generate data
objects based on these.  The underlying bits of data are stored in Scheme
bytevectors.  Access to component data is provided by the
<code class="code">cdata-ref</code> procedure and mutation is 
accomplished via the <code class="code">cdata-set!</code> procedure.  
The modules support non-native machine architectures via a
global parameter called <code class="code">*arch*</code>.
</p>
<p>Beyond size and
alignment, base type objects carry a symbolic tag to determine the
appropriate low level machine type.
The low level machine types map directly to bytevector setters
and getters.  Support for C base types is handled by the <code class="code">cbase</code>
procedure which converts them to underlying types.  For example, on a
64 bit little endian architecture, <code class="code">(cbase 'uintptr_t)</code> would
generate a type with underlying symbol <code class="code">u64le</code>.
</p>
<p>Here is a simple example of using <em class="emph">cdata</em> for structures:
</p>
<div class="example">
<pre class="example-preformatted">(use-modules (system foreign))
(use-modules (system foreign-library))
(use-modules (nyacc foreign cdata))

(define timeval_t (cstruct '((tv_sec long) (tv_usec long))))

(define gettimeofday
  (foreign-library-function
   #f &quot;gettimeofday&quot;
   #:return-type (ctype-&gt;ffi (cbase 'int))
   #:arg-types (map ctype-&gt;ffi
                    (list (cpointer timeval_t)
                          (cpointer 'void)))))

(define d1 (make-cdata timeval_t))
(gettimeofday (cdata-ref (cdata&amp; d1)) %null-pointer)
(format #t &quot;time: ~s ~s\n&quot;
        (cdata-ref d1 'tv_sec) (cdata-ref d1 'tv_usec))
time: 1719062561 676365
</pre></div>
<p>In the above <code class="code">cdata&amp;</code> generates a cdata pointer to <code class="code">d1</code> and
<code class="code">cdata-ref</code> extracts the Guile value.
</p>
<h3 class="heading" id="Basic-Usage"><span>Basic Usage<a class="copiable-link" href="#Basic-Usage"> &para;</a></span></h3>

<p>This section provides an introduction to procedures you are likely to
want on your first approach.
</p>
<dl class="first-deffn">
<dt class="deffn" id="index-cbase"><span class="category-def">Procedure: </span><span><strong class="def-name">cbase</strong> <var class="def-var-arguments">name</var><a class="copiable-link" href="#index-cbase"> &para;</a></span></dt>
<dd><p>Given symbolic <var class="var">name</var> generate a base ctype.   The name can
be a symbol like <code class="code">unsigned-int</code>, <code class="code">double</code>, or can be a
<em class="emph">arch-info</em> machine type symbol like <code class="code">u64le</code>.
</p></dd></dl>

<dl class="first-deffn">
<dt class="deffn" id="index-cpointer"><span class="category-def">Procedure: </span><span><strong class="def-name">cpointer</strong> <var class="def-var-arguments">type =&gt; &lt;ctype&gt;</var><a class="copiable-link" href="#index-cpointer"> &para;</a></span></dt>
<dd><p>Generate a C pointer type for <var class="var">type</var>. To reference or de-reference
cdata object see <code class="code">cdata&amp;</code> and <code class="code">cdata*</code>.  <var class="var">type</var> can be
the symbol <code class="code">void</code> or a symbolic name used as argument to <code class="code">cbase</code>.
<br>note: Should we allow <var class="var">type</var> to be a promise?
</p><div class="example">
<pre class="example-preformatted">(define foo_t (cbase 'int))
(cpointer (delay foo_t))
</pre></div>
</dd></dl>

<dl class="first-deffn">
<dt class="deffn" id="index-cstruct"><span class="category-def">Procedure: </span><span><strong class="def-name">cstruct</strong> <var class="def-var-arguments">fields [packed] =&gt; ctype</var><a class="copiable-link" href="#index-cstruct"> &para;</a></span></dt>
<dd><p>Construct a struct ctype with given <var class="var">fields</var>.  If <var class="var">packed</var>,
<code class="code">#f</code> by default, is <code class="code">#t</code>, create a packed structure.
<var class="var">fields</var> is a list with entries of the form <code class="code">(name type)</code> or
<code class="code">(name type lenth)</code> where <code class="code">name</code> is a symbol or <code class="code">#f</code>
(for anonymous structs and unions), <code class="code">type</code> is a <code class="code">&lt;ctype&gt;</code>
object or a symbol for a base type and <code class="code">length</code> is the length
of the associated bitfield.
</p></dd></dl>

<dl class="first-deffn">
<dt class="deffn" id="index-cunion"><span class="category-def">Procedure: </span><span><strong class="def-name">cunion</strong> <var class="def-var-arguments">fields =&gt; &lt;ctype&gt;</var><a class="copiable-link" href="#index-cunion"> &para;</a></span></dt>
<dd><p>Construct a ctype union type with given <var class="var">fields</var>.
See <em class="emph">cstruct</em> for a description of the <var class="var">fields</var> argument.
</p></dd></dl>

<dl class="first-deffn">
<dt class="deffn" id="index-carray"><span class="category-def">Procedure: </span><span><strong class="def-name">carray</strong> <var class="def-var-arguments">type n =&gt; &lt;ctype&gt;</var><a class="copiable-link" href="#index-carray"> &para;</a></span></dt>
<dd><p>Create an array of <var class="var">type</var> with <var class="var">length</var>.
If <var class="var">length</var> is zero, the array length is unbounded: it&rsquo;s length
can be specified as argument to <code class="code">make-cdata</code>.
</p></dd></dl>


<dl class="first-deffn">
<dt class="deffn" id="index-cenum"><span class="category-def">Procedure: </span><span><strong class="def-name">cenum</strong> <var class="def-var-arguments">enum-list [packed] =&gt; &lt;ctype&gt;</var><a class="copiable-link" href="#index-cenum"> &para;</a></span></dt>
<dd><p><var class="var">enum-list</var> is a list of name or name-value pairs
</p><div class="example">
<pre class="example-preformatted">(cenum '((a 1) b (c 4))
</pre></div>
<p>If <var class="var">packed</var> is <code class="code">#t</code> the size will be smallest that can hold it.
</p></dd></dl>


<dl class="first-deffn">
<dt class="deffn" id="index-cfunction"><span class="category-def">Procedure: </span><span><strong class="def-name">cfunction</strong> <var class="def-var-arguments">proc-&gt;ptr ptr-&gt;proc [variadic?] =&gt; &lt;ctype&gt;</var><a class="copiable-link" href="#index-cfunction"> &para;</a></span></dt>
<dd><p>Generate a C function type to be used with <code class="code">cpointer</code>.  You must
pass the <var class="var">wrapper</var> and <var class="var">unwrapper</var> procedures that convert a
procedure to a pointer, and pointer to procedure, respectively.  The
optional argument <var class="var">#:variadic</var>, if <code class="code">#t</code>,  indicates the function
uses variadic arguments.  For this case (I need to add documention).
Here is an example:
</p><div class="example">
<pre class="example-preformatted">(define (f-proc-&gt;ptr proc)
  (ffi:procedure-&gt;pointer ffi:void proc (list)))
(define (f-ptr-&gt;proc fptr)
  (ffi:pointer-&gt;procedure ffi:void fptr (list)))
(define ftype (cpointer (cfunction f-proc-&gt;ptr f-ptr-&gt;proc)))
</pre></div>
</dd></dl>

<dl class="first-deffn">
<dt class="deffn" id="index-make_002dcdata"><span class="category-def">Procedure: </span><span><strong class="def-name">make-cdata</strong> <var class="def-var-arguments">type [value] =&gt; &lt;cdata&gt;</var><a class="copiable-link" href="#index-make_002dcdata"> &para;</a></span></dt>
<dd><p>Generate a <em class="emph">cdata</em> object of type <var class="var">type</var> with optional <var class="var">value</var>.
As a special case, an integer arg to a zero-sized array type will allocate
storage for that many items, associating it with an array type of that size.
</p></dd></dl>

<dl class="first-deffn">
<dt class="deffn" id="index-cdata_002dref"><span class="category-def">Procedure: </span><span><strong class="def-name">cdata-ref</strong> <var class="def-var-arguments">data [tag ...] =&gt; value</var><a class="copiable-link" href="#index-cdata_002dref"> &para;</a></span></dt>
<dd><p>Return the Scheme (scalar) slot value for selected <var class="var">tag ...</var> with
respect to the cdata object <var class="var">data</var>.
</p><div class="example">
<pre class="example-preformatted">(cdata-ref my-struct-value 'a 'b 'c))
</pre></div>
<p>This procedure returns Guile values for cdata kinds <em class="emph">base</em>,
<em class="emph">pointer</em> and <em class="emph">procedure</em>.   For other cases, a <em class="emph">cdata</em>
object is returned.  If you always want a cdata object, use <code class="code">cdata-sel</code>.
</p></dd></dl>


<dl class="first-deffn">
<dt class="deffn" id="index-cdata_002dset_0021"><span class="category-def">Procedure: </span><span><strong class="def-name">cdata-set!</strong> <var class="def-var-arguments">data value [tag ...]</var><a class="copiable-link" href="#index-cdata_002dset_0021"> &para;</a></span></dt>
<dd><p>Set slot for selcted <var class="var">tag ...</var> with respect to cdata <var class="var">data</var> to
<var class="var">value</var>.  Example:
</p><div class="example">
<pre class="example-preformatted">(cdata-set! my-struct-data 42 'a 'b 'c))
</pre></div>
<p>If <var class="var">value</var> is a <code class="code">&lt;cdata&gt;</code> object copy that, if types match.
<br>If <var class="var">value</var> can be a procedure used to set a cfunction pointer
value.
</p></dd></dl>

<dl class="first-deffn">
<dt class="deffn" id="index-cdata_0026"><span class="category-def">Procedure: </span><span><strong class="def-name">cdata&amp;</strong> <var class="def-var-arguments">data =&gt; cdata</var><a class="copiable-link" href="#index-cdata_0026"> &para;</a></span></dt>
<dd><p>Generate a reference (i.e., cpointer) to the contents in the underlying
bytevector.
</p></dd></dl>

<dl class="first-deffn">
<dt class="deffn" id="index-cdata_002a"><span class="category-def">Procedure: </span><span><strong class="def-name">cdata*</strong> <var class="def-var-arguments">data =&gt; cdata</var><a class="copiable-link" href="#index-cdata_002a"> &para;</a></span></dt>
<dd><p>De-reference a pointer.  Returns a <em class="emph">cdata</em> object representing the
contents at the address in the underlying bytevector.
</p></dd></dl>

<h3 class="heading" id="Going-Further"><span>Going Further<a class="copiable-link" href="#Going-Further"> &para;</a></span></h3>

<dl class="first-deffn">
<dt class="deffn" id="index-cdata_002dsel"><span class="category-def">Procedure: </span><span><strong class="def-name">cdata-sel</strong> <var class="def-var-arguments">data tag ... =&gt; cdata</var><a class="copiable-link" href="#index-cdata_002dsel"> &para;</a></span></dt>
<dd><p>Return a new <code class="code">cdata</code> object representing the associated selection.
Note this is different from <code class="code">cdata-ref</code>: it always returns a cdata
object.  For example,
</p><div class="example">
<pre class="example-preformatted">&gt; (define t1 (cstruct '((a int) (b double))))
&gt; (define d1 (make-cdata t1))
&gt; (cdata-set! d1 42 'a)
&gt; (cdata-sel d1 'a)
$1 = #&lt;cdata s32le 0x77bbf8e52260&gt;
&gt; (cdata-ref $1)
$2 = 42
</pre></div>
</dd></dl>

<dl class="first-deffn">
<dt class="deffn" id="index-cdata_0026_002dref"><span class="category-def">Procedure: </span><span><strong class="def-name">cdata&amp;-ref</strong> <var class="def-var-arguments">data [tag ...] =&gt; value</var><a class="copiable-link" href="#index-cdata_0026_002dref"> &para;</a></span></dt>
<dd><p>Shortcut for <code class="code">(cdata-ref (cdata&amp; data tag ...))</code>
This always returns a Guile <em class="emph">pointer</em>.
</p></dd></dl>

<dl class="first-deffn">
<dt class="deffn" id="index-cdata_002a_002dref"><span class="category-def">Procedure: </span><span><strong class="def-name">cdata*-ref</strong> <var class="def-var-arguments">data [tag ...] =&gt; value</var><a class="copiable-link" href="#index-cdata_002a_002dref"> &para;</a></span></dt>
<dd><p>Shortcut for <code class="code">(cdata-ref (cdata* data tag ...))</code>
</p></dd></dl>

<dl class="first-deffn">
<dt class="deffn" id="index-Xcdata_002dref"><span class="category-def">Procedure: </span><span><strong class="def-name">Xcdata-ref</strong> <var class="def-var-arguments">bv ix ct -&gt; value</var><a class="copiable-link" href="#index-Xcdata_002dref"> &para;</a></span></dt>
<dd><p>Reference a deconstructed cdata object. See <em class="emph">cdata-ref</em>.
</p></dd></dl>

<dl class="first-deffn">
<dt class="deffn" id="index-Xcdata_002dset_0021"><span class="category-def">Procedure: </span><span><strong class="def-name">Xcdata-set!</strong> <var class="def-var-arguments">bv ix ct value</var><a class="copiable-link" href="#index-Xcdata_002dset_0021"> &para;</a></span></dt>
<dd><p>Set the value of a deconstructed cdata object. See <em class="emph">cdata-set!</em>.
</p></dd></dl>

<h3 class="heading" id="Working-with-Types"><span>Working with Types<a class="copiable-link" href="#Working-with-Types"> &para;</a></span></h3>

<dl class="first-deffn">
<dt class="deffn" id="index-name_002dctype"><span class="category-def">Procedure: </span><span><strong class="def-name">name-ctype</strong> <var class="def-var-arguments">name type =&gt; &lt;ctype&gt;</var><a class="copiable-link" href="#index-name_002dctype"> &para;</a></span></dt>
<dd><p>Create a new named version of the type.  The name is useful when the type
is printed.  This procedure does not mutate: a new type object is created.
If a specific type is used by multiple names the names can share
the underlying type guts.  The following generates two named types.
</p><div class="example">
<pre class="example-preformatted">(define raw (cstruct '((a 'int) (b 'double))))
(define foo_t (name-ctype 'foo_t raw))
(define struct-foo (name-ctype 'struct-foo raw))
</pre></div>
<p>These types are equal:
</p><div class="example">
<pre class="example-preformatted">(ctype-equal? foo_t struct-foo) =&gt; #t
</pre></div>
</dd></dl>

<dl class="first-deffn">
<dt class="deffn" id="index-ctype_002dequal_003f"><span class="category-def">Procedure: </span><span><strong class="def-name">ctype-equal?</strong> <var class="def-var-arguments">a b</var><a class="copiable-link" href="#index-ctype_002dequal_003f"> &para;</a></span></dt>
<dd><p>This predicate assesses equality of it&rsquo;s arguments.
Two types are considered equal if they have the same size,
alignment, kind, and eqivalent kind-specific properties.
For base types, the symbolic mtype must be equal; this includes
size, integer versus float, and signed versus unsigned.
For struct and union kinds, the names and types of all fields
must be equal.
<br>TODO: algorithm to prevent infinite search for recursive structs
</p></dd></dl>

<dl class="first-deffn">
<dt class="deffn" id="index-ctype_002dsel"><span class="category-def">Procedure: </span><span><strong class="def-name">ctype-sel</strong> <var class="def-var-arguments">type ix [tag ...] =&gt; ((ix . ct) (ix . ct) ...)</var><a class="copiable-link" href="#index-ctype_002dsel"> &para;</a></span></dt>
<dd><p>This generate a list of (offset, type) pairs for a type.  The result is
used to create getters and setter for foreign machine architectures.
See <em class="emph">make-cdata-getter</em> and <em class="emph">make-cdata-setter</em>.
</p></dd></dl>

<dl class="first-deffn">
<dt class="deffn" id="index-make_002dcdata_002dgetter"><span class="category-def">Procedure: </span><span><strong class="def-name">make-cdata-getter</strong> <var class="def-var-arguments">sel [offset] =&gt; lambda</var><a class="copiable-link" href="#index-make_002dcdata_002dgetter"> &para;</a></span></dt>
<dd><p>Genererate a procedure that given a cdata object will fetch the value
at indicated by the <var class="var">sel</var>, generated by <code class="code">ctype-sel</code>.
The procedure takes one argument: <code class="code">(proc data [tag ...])</code>.
Pointer dereference tags (<code class="code">'*'</code>) are not allowed.
The optional <var class="var">offset</var> argument (default 0), is used for cross
target use: it is the offset of the address in the host context.
</p></dd></dl>

<dl class="first-deffn">
<dt class="deffn" id="index-make_002dcdata_002dsetter"><span class="category-def">Procedure: </span><span><strong class="def-name">make-cdata-setter</strong> <var class="def-var-arguments">sel [offset] =&gt; lambda</var><a class="copiable-link" href="#index-make_002dcdata_002dsetter"> &para;</a></span></dt>
<dd><p>Genererate a procedure that given a cdata object will set the value
at the offset given the selector, generated by <code class="code">ctype-sel</code>.
The procedure takes two arguments: <code class="code">(proc data value [tag ...])</code>.
Pointer dereference tags (<code class="code">'*'</code>) are not allowed.
The optional <var class="var">offset</var> argument (default 0), is used for cross
target use: it is the offset of the address in the host context.
</p></dd></dl>


<h3 class="heading" id="Working-with-C-Function-Calls"><span>Working with C Function Calls<a class="copiable-link" href="#Working-with-C-Function-Calls"> &para;</a></span></h3>

<p>The procedure
<code class="code">ctype-&gt;ffi</code> is a helper for using Guile&rsquo;s
<em class="emph">pointer-&gt;procedure</em>.
</p>
<dl class="first-deffn">
<dt class="deffn" id="index-ccast"><span class="category-def">Procedure: </span><span><strong class="def-name">ccast</strong> <var class="def-var-arguments">type data [do-check] =&gt; &lt;cdata&gt;</var><a class="copiable-link" href="#index-ccast"> &para;</a></span></dt>
<dd><p>need to be able to cast array to pointer
</p><div class="example">
<pre class="example-preformatted">(ccast Target* val)
</pre></div>
</dd></dl>

<dl class="first-deffn">
<dt class="deffn" id="index-unwrap_002dnumber"><span class="category-def">Procedure: </span><span><strong class="def-name">unwrap-number</strong> <var class="def-var-arguments">arg =&gt; number</var><a class="copiable-link" href="#index-unwrap_002dnumber"> &para;</a></span></dt>
<dd><p>Convert an argument to numeric form for a ffi procedure call.
This will reference a cdata object or pass a number through.
</p></dd></dl>


<dl class="first-deffn">
<dt class="deffn" id="index-unwrap_002dpointer"><span class="category-def">Procedure: </span><span><strong class="def-name">unwrap-pointer</strong> <var class="def-var-arguments">arg [hint] =&gt; pointer</var><a class="copiable-link" href="#index-unwrap_002dpointer"> &para;</a></span></dt>
<dd><p>Convert an argument to a Guile pointer for a ffi procedure call.
This will reference a cdata object or pass a number through.
If the argument is a function, it will attempt to convert that
to a pointer via <code class="code">procedure-&gt;pointer</code> if given the function
pointer type <var class="var">hint</var>.
</p></dd></dl>

<dl class="first-deffn">
<dt class="deffn" id="index-unwrap_002darray"><span class="category-def">Procedure: </span><span><strong class="def-name">unwrap-array</strong> <var class="def-var-arguments">arg =&gt; pointer</var><a class="copiable-link" href="#index-unwrap_002darray"> &para;</a></span></dt>
<dd><p>This will convert an array to a form suitable to pass to a Guile
ffi procedure.
</p></dd></dl>

<dl class="first-deffn">
<dt class="deffn" id="index-ctype_002d_003effi"><span class="category-def">Procedure: </span><span><strong class="def-name">ctype-&gt;ffi</strong> <var class="def-var-arguments">ctype =&gt; ffi type</var><a class="copiable-link" href="#index-ctype_002d_003effi"> &para;</a></span></dt>
<dd><p>Generate a argument spec for Guile&rsquo;s ffi interface.
Example:
</p><div class="example">
<pre class="example-preformatted">(ctype-&gt;ffi (cpointer (cbase int))) =&gt; '*
</pre></div>
</dd></dl>

<h3 class="heading" id="Operations-on-CType-Kinds"><span>Operations on CType Kinds<a class="copiable-link" href="#Operations-on-CType-Kinds"> &para;</a></span></h3>

<p>The ctype <code class="code">kind</code> field indicates which kind a type is and
the <code class="code">info</code> field provide kind-specific information for a ctype.
The <code class="code">name</code> field provides the type name, if provided, or
<code class="code">#f</code> if not.
</p>
<p>Note that the kind procecures, <code class="code">cstruct</code>, <code class="code">cpointer</code>, ...,
create <em class="emph">ctype</em> objects of different <em class="emph">kinds</em>.  To operate
on kind-specific attributes of types, requries one to fetch the
<code class="code">info</code> field from the ctype.  From the <code class="code">info</code> field, one
can then operate using the fields specific to the kind info.
</p>
<div class="example">
<pre class="example-preformatted">&gt; (define float* (cpointer (cbase 'float)))
&gt; double*
$1 = #&lt;ctype pointer 0x75f3212cbed0&gt;
&gt; (ctype-kind float*)
$2 = pointer
&gt; (define float*-info (ctype-info float*))
&gt; (cpointer-type float*-info)
$3 = #&lt;ctype f32le 0x75f323f8ec90&gt;
&gt; (cpointer-mtype float*-info)
$4 = u64le
</pre></div>
<p>The <code class="code">cpointer-mtype</code> procedure lets us know that pointers are
stored as unsigned 64 bit (little endian) integers.
</p>
<p>The info field for base types is special.  Since the only
kind-specific type information for a base type is the machine type
the info field provides that.  Consider the following example.
</p><div class="example">
<pre class="example-preformatted">&gt; (define foo-t (name-ctype 'foo-t (cbase 'int)))
&gt; (ctype-name foo-t)
$1 = foo-t
&gt; (ctype-kind foo-t)
$2 = base
&gt; (ctype-info foo-t)
$3 = s32le
</pre></div>

<p>Structs are more involved.
</p><div class="example">
<pre class="example-preformatted">&gt; (define bar-s
    (cstruct `((a int) (b float) (#f ,(cstruct '(x int) (y int))))))
&gt; (define bar-s-info (ctype-info bar-s))
&gt; (cstruct-fields bar-s-info)
$4 = (#&lt;&lt;cfield&gt; name: a type: #&lt;ctype s32le 0x75f323f8ecf0&gt; offset: 0&gt;
      #&lt;&lt;cfield&gt; name: b type: #&lt;ctype f32le 0x75f323f8ec90&gt; offset: 4&gt;
      #&lt;&lt;cfield&gt; name: #f type: #&lt;ctype struct 0x75f32181a570&gt; offset: 8&gt;)
&gt; (define x-fld ((cstruct-select bar-s-info) 'x))
&gt; x-fld
$5 = #&lt;&lt;cfield&gt; name: x type: #&lt;ctype s32le 0x75f323f8ecf0&gt; offset: 8&gt;
&gt; (cfield-offset x-fld)
$6 = 8
</pre></div>
<p>Note that the selection of the <code class="code">x</code> component deals with a
field which is an anonymous struct. The struct <code class="code">bar-s</code> would look
like the following in C:
</p><div class="example">
<pre class="example-preformatted">struct bar_s {
  int a;
  float b;
  struct {
    int x;
    int y;
  };
};
</pre></div>

<p>And just for kicks
</p><div class="example">
<pre class="example-preformatted">&gt; (define sa 
    (cstruct `((a int) (b double) (#f ,(cstruct '((x short) (y int)))))))
&gt; (define sp 
    (cstruct `((a int) (b double) (#f ,(cstruct '((x short) (y int))))) #t))

&gt; (pretty-print-ctype sa)
(cstruct
  ((a s32le #:offset 0)
   (b f64le #:offset 8)
   (#f
    (cstruct
      ((x s16le #:offset 0) (y s32le #:offset 4)))
    #:offset
    16)))
&gt; (pretty-print-ctype sp)
(cstruct
  ((a s32le #:offset 0)
   (b f64le #:offset 4)
   (#f
    (cstruct
      ((x s16le #:offset 0) (y s32le #:offset 4)))
    #:offset
    12)))
</pre></div>
<p>Note the difference in offsets: <code class="code">sa</code> is aligned and <code class="code">sp</code> is
packed.  The offsets reported for anonymous structs can be misleading.
To get the right offsets use select:
</p><div class="example">
<pre class="example-preformatted">&gt; (define tia (ctype-info sa))
&gt; (define tip (ctype-info sp))
&gt; ((cstruct-select tia) 'y)
$8 = 20
&gt; ((cstruct-select tip) 'y)
$9 = 16
</pre></div>

<h4 class="subheading" id="Enum-Conversions"><span>Enum Conversions<a class="copiable-link" href="#Enum-Conversions"> &para;</a></span></h4>

<p>The enum ctype provides procedures to convert between the numeric and
symbolic parts of each enum entry.   Currently, the cdata module does
not provide enum wrapper and unwrapper routines.  However, the FFI
Helper will create these.   The wrapper, converting a number to a
symbol, and unwrapper, converting a symbol to a number, can be
generated as the following example demonstrates.
</p>
<div class="example">
<pre class="example-preformatted">&gt; (define color_t (cenum '((RED #xf00) (GREEN #x0f0) (BLUE #x00f))))
&gt; (define color_t-info (ctype-info color_t))
&gt; (define wrap-color_t (cenum-symf color_t-info))
&gt; (define unwrap-color_t (cenum-numf color_t-info))
&gt; (wrap-color_t #xf00)
$1 = RED
&gt; (unwrap-color_t 'GREEN)
$2 = 240
</pre></div>


<h3 class="heading" id="Handling-Machine-Architectures"><span>Handling Machine Architectures<a class="copiable-link" href="#Handling-Machine-Architectures"> &para;</a></span></h3>

<p>One of the author&rsquo;s main motivations for writing CData was to be able
to work with cross-target machine architectures.   This is pretty
cool.  Just to let you know what&rsquo;s going on, consider the following:
</p>
<div class="example">
<pre class="example-preformatted">&gt; (use-modules (nyacc foreign arch-info))
&gt; (define tx64 (with-arch &quot;x86_64&quot; (cstruct '((a int) (b long)))))
&gt; (define tr64 (with-arch &quot;riscv64&quot; (cstruct '((a int) (b long)))))
&gt; (define tr32 (with-arch &quot;riscv32&quot; (cstruct '((a int) (b long)))))
&gt; (define sp32 (with-arch &quot;sparc&quot; (cstruct '((a int) (b long)))))
&gt; (ctype-equal? tx64 tr64)
$1 = #t
&gt; (ctype-equal? tr64 tr32)
$1 = #f
&gt; (ctype-equal? tr32 ts32)
$1 = #f
&gt; (pretty-print-ctype tx64)
(cstruct ((a s32le #:offset 0) (b s64le #:offset 8)))
&gt; (pretty-print-ctype tr64)
(cstruct ((a s32le #:offset 0) (b s64le #:offset 8)))
&gt; (pretty-print-ctype tr32)
(cstruct ((a s32le #:offset 0) (b s32le #:offset 4)))
&gt; (pretty-print-ctype ts32)
(cstruct ((a s32be #:offset 0) (b s32be #:offset 4)))
</pre></div>

<p>Rocks, right?
</p>
<p>arch-info maps base C types to machine types (e.g., i32le) and
alignment for the given machine architecture.    To get sizes, it&rsquo;s a
simple matter of mapping machine types to sizes.
</p>
<p>The arch-info module currently has size and alignment information for
the for the following: aarch64, avr, i383, i686, powerpc32, powerpc64,
ppc32, ppc64, riscv32, riscv64, sparc32, sparc64, x86_64.
</p>

<h3 class="heading" id="CData-Utilities"><span>CData Utilities<a class="copiable-link" href="#CData-Utilities"> &para;</a></span></h3>

<dl class="first-deffn">
<dt class="deffn" id="index-pretty_002dprint_002dctype"><span class="category-def">Procedure: </span><span><strong class="def-name">pretty-print-ctype</strong> <var class="def-var-arguments">type [port]</var><a class="copiable-link" href="#index-pretty_002dprint_002dctype"> &para;</a></span></dt>
<dd><p>Converts type to a literal tree and uses Guile&rsquo;s pretty-print function
to display it.  The default port is the current output port.
</p></dd></dl>

<dl class="first-deffn">
<dt class="deffn" id="index-cdata_002dkind"><span class="category-def">Procedure: </span><span><strong class="def-name">cdata-kind</strong> <var class="def-var-arguments">data</var><a class="copiable-link" href="#index-cdata_002dkind"> &para;</a></span></dt>
<dd><p>Return the kind of <var class="var">data</var>: pointer, base, struct, ...
</p></dd></dl>

<h3 class="heading" id="Miscellaneous"><span>Miscellaneous<a class="copiable-link" href="#Miscellaneous"> &para;</a></span></h3>

<p>More to come.
</p>
<h4 class="subheading" id="Base-Types"><span>Base Types<a class="copiable-link" href="#Base-Types"> &para;</a></span></h4>

<div class="example">
<pre class="example-preformatted">void*
char short int long float double unsigned-short unsigned unsigned-long
size_t ssize_t ptrdiff_t int8_t uint8_t int16_t uint16_t int32_t
uint32_t int64_t uint64_t signed-char unsigned-char short-int
signed-short signed-short-int signed signed-int long-int signed-long
signed-long-int unsigned-short-int unsigned-int unsigned-long-int
_Bool bool intptr_t uintptr_t wchar_t char16_t char32_t long-double
long-long long-long-int signed-long-long signed-long-long-int
unsigned-long-long unsigned-long-long-int
</pre></div>

<h4 class="subheading" id="Other-Procedures"><span>Other Procedures<a class="copiable-link" href="#Other-Procedures"> &para;</a></span></h4>

<p>More to come.
</p>

<h4 class="subheading" id="Guile-FFI-Support"><span>Guile FFI Support<a class="copiable-link" href="#Guile-FFI-Support"> &para;</a></span></h4>

<p>More to come.
</p>
<dl class="first-deffn">
<dt class="deffn" id="index-ctype_002d_003effi_002dtype"><span class="category-def">Procedure: </span><span><strong class="def-name">ctype-&gt;ffi-type</strong> <var class="def-var-arguments">type</var><a class="copiable-link" href="#index-ctype_002d_003effi_002dtype"> &para;</a></span></dt>
<dd><p>Convert a <em class="emph">ctype</em> to the (integer) code for the associated FFI
type.
</p></dd></dl>

<h4 class="subheading" id="Copyright"><span>Copyright<a class="copiable-link" href="#Copyright"> &para;</a></span></h4>

<p>Copyright (C) 2024 &ndash; Matthew Wette.
</p>
<p>Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3 or
any later version published by the Free Software Foundation; with no
Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.  A
copy of the license is included with the distribution as COPYING.DOC.
</p>
<h3 class="heading" id="References"><span>References<a class="copiable-link" href="#References"> &para;</a></span></h3>

<ol class="enumerate">
<li> Guile Manual:
<a class="url" href="https://www.gnu.org/software/guile/manual">https://www.gnu.org/software/guile/manual</a>
</li><li> Scheme Bytestructures:
<a class="url" href="https://github.com/TaylanUB/scheme-bytestructures">https://github.com/TaylanUB/scheme-bytestructures</a>
</li></ol>

</div>



</body>
</html>
